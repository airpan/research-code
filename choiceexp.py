from learning_theano.generate_net import load_kl_net
from cfour import ConnectFourState
from montecarlo import UCT, normsample, softmax, depthUCT, NUM_PLAYOUTS, \
        makeDefaultFromStochastic
from math import *
import numpy as np
import random

sto_policy = load_kl_net("sto_net_params/playerflip/depthUCT1000fixed")
s = ConnectFourState()

def kl(v1, v2):
    tot = 0
    for p, q in zip(v1, v2):
        if p == 0:
            continue
        tot += p * (log(p) - log(q))
    return tot

def softmaxtree(sto_policy):
    def pol(node, state):
        vals = [float('-inf')] * 7
        raw = [(0,0)] * 7
        acts = set()
        for c in node.childNodes:
            vals[c.lastAction] = c.qvalue / float(c.visits)
            raw[c.lastAction] = (c.qvalue, c.visits)
            acts.add(c.lastAction)
        vals = np.array(vals)
        probs = softmax(vals)
        predicted = sto_policy([state.toFeatures()])[0]
        base = kl(probs, predicted)

        # estimate change for playing action i
        # base from online Q-value
        errs = [0] * 7
        for a in acts:
            qv = 0.5 * (vals[a] + 1)
            v2 = np.array(vals)
            v2[a] = (raw[a][0]+1) / (float(raw[a][1]) + 1)
            tot = qv * kl(softmax(v2), predicted)
            v2 = np.array(vals)
            v2[a] = (raw[a][0]-1) / (float(raw[a][1]) + 1)
            tot += (1-qv) * kl(softmax(v2), predicted)
            errs[a] = tot
        a = max(acts, key=lambda i: errs[i])
        state.doAction(a)
        return node.childMap[a]
    return pol

# choose probabilistically based on raw difference?
def softmaxtree2(sto_policy):
    def pol(node, state):
        vals = [float('-inf')] * 7
        raw = [(0,0)] * 7
        acts = set()
        for c in node.childNodes:
            vals[c.lastAction] = c.qvalue / float(c.visits)
            raw[c.lastAction] = (c.qvalue, c.visits)
            acts.add(c.lastAction)
        vals = np.array(vals)
        probs = softmax(vals)
        predicted = sto_policy([state.toFeatures()])[0]
        diffs = [float('-inf')] * 7
        for a in acts:
            diffs[a] = abs(probs[a] - predicted[a])
        final_dist = softmax(np.array(diffs), beta=2)
        r = random.random()
        for a,p in enumerate(final_dist):
            if r < p:
                state.doAction(a)
                return node.childMap[a]
            r -= p
    return pol

v, a = UCT(
    s,
    NUM_PLAYOUTS,
    treePolicy=depthUCT,
    defaultPolicy=makeDefaultFromStochastic(sto_policy),
    returnValues=True
)

v2, a2 = UCT(
    s,
    NUM_PLAYOUTS,
    treePolicy=softmaxtree2(sto_policy),
    defaultPolicy=makeDefaultFromStochastic(sto_policy),
    returnValues=True
)
