from math import *
from game import GameState
from util import Counter
import platform
# check if running in PyPy
USING_PYPY = platform.python_implementation() == 'PyPy'

if USING_PYPY:
    pass
else:
    import numpy as np
    import theano
import random

# Game state implementation for connect four

class ConnectFourState(GameState):
    WIDTH = 7
    HEIGHT = 6
    def __init__(self, player=1):
        """
        Player = 1 or 2
        board = 0 for empty, 1 for player 1 piece, 2 for player 2 piece
        actions = a column number from 0 to 6
        reward = +1 for player 1 win, -1 for player 2 win, 0 for a draw
        """
        GameState.__init__(self, player)
        self.board = [ [0] * self.HEIGHT for _ in xrange(self.WIDTH) ]
        self.history = []

    def doAction(self, action):
        # modifies the state in place and returns self
        for i in xrange(self.HEIGHT-1, -1, -1):
            if self.board[action][i] == 0:
                self.board[action][i] = self.player
                break
        self.reward = None
        self.player = 3 - self.player
        self.history.append(action)
        return self

    def reverseAction(self):
        action = self.history.pop()
        for i in xrange(self.HEIGHT):
            if self.board[action][i] != 0:
                self.board[action][i] = 0
                break
        self.player = 3 - self.player
        return self

    def reverseActions(self, n):
        for _ in xrange(n):
            self.reverseAction()

    def getActions(self):
        if self.isTerminal():
            return []
        return [i for i in xrange(self.WIDTH) if self.board[i][0] == 0]

    def isLegal(self, action):
        return self.board[action][0] == 0

    def isTerminal(self):
        # only need to check last piece added
        if len(self.history) == 0:
            return False
        i = self.history[-1]
        j = 0
        while self.board[i][j] == 0:
            j += 1

        # Horizontals
        # x = start positions
        # last possible start is either i or 3
        for x in xrange(max(0, i-3), min(self.WIDTH - 3, i+1)):
            if self.board[x][j] == self.board[x+1][j] == self.board[x+2][j] == self.board[x+3][j]:
                #print 'hori', i, j ,x
                return True

        # Verticals
        # we can assume the vertical ends at (i, j)
        if j +3 < self.HEIGHT:
            if self.board[i][j] == self.board[i][j+1] == self.board[i][j+2] == self.board[i][j+3]:
                return True

        # Diag going to top right
        # x and y are bottom most piece of diag
        x = i - 3
        y = j + 3
        while x < 0 or y >= self.HEIGHT:
            x += 1
            y -= 1
        while x < self.WIDTH - 3 and y >= 3 and x <= i and y >= j:
            if self.board[x][y] == self.board[x+1][y-1] == self.board[x+2][y-2] == self.board[x+3][y-3]:
                #print 'diag right', i,j,x,y
                return True
            x += 1
            y -= 1

        # Diag going top left
        # x and y are also bottom most piece
        x = i + 3
        y = j + 3
        while x >= self.WIDTH or y >= self.HEIGHT:
            x -= 1
            y -= 1
        while x >= 3 and y >= 3 and x >= i and y >= j:
            if self.board[x][y] == self.board[x-1][y-1] == self.board[x-2][y-2] == self.board[x-3][y-3]:
                #print 'diag left', i,j,x,y
                return True
            x -= 1
            y -= 1

        # Draw case
        for i in xrange(self.WIDTH):
            if self.board[i][0] == 0:
                return False
        # board is full
        self.reward = 0
        return True

    def getReward(self):
        # Zero sum game, 1 = win for maxmizer, -1 = win for minimizer
        # next player is losing player if this is terminal
        if self.reward is not None:
            return self.reward
        return 1 if self.player == 2 else -1

    def getRewardForAction(self, action):
        n = self.doAction(action)
        if not n.isTerminal():
            return 0
        return n.getReward()

    def getWinner(self):
        if self.getReward() == 0:
            return 0
        return 3 - self.player

    def toFeatures(self):
        # just use the values of the squares
        cells = reduce(lambda x,y: x + y, self.board)
        def f(n):
            if n == 0:
                return 0
            if n == 1:
                return 1
            return -1
        if USING_PYPY:
            return [f(c) for c in cells]
        else:
            return np.array([f(c) for c in cells], dtype=theano.config.floatX)

    def toFlippedFeatures(self):
        # return the exact opposite convention as the above
        # use when we need to pretend player 2 is player 1
        cells = reduce(lambda x,y: x + y, self.board)
        def f(n):
            if n == 0:
                return 0
            if n == 1:
                return -1
            return 1
        if USING_PYPY:
            return [f(c) for c in cells]
        else:
            return np.array([f(c) for c in cells], dtype=theano.config.floatX)

    def __str__(self):
        to_print = ['0123456']
        for j in xrange(self.HEIGHT):
            row = (self.board[i][j] for i in xrange(self.WIDTH))
            to_print.append(''.join('.' if c == 0 else 'O' if c == 1 else 'X' for c in row))
        return '\n'.join(to_print)


# This state is probably too simple to be worth diong anything with
class TicTacToeState(GameState):

    def __init__(self, player=1, board=None):
        GameState.__init__(self, player)
        self.board = board or [0]*9

    def _calculateActions(self):
        # Actions = a number from 0 to 8
        actions = []
        for i in xrange(9):
            if self.board[i] == 0:
                actions.append(i)
        return actions

    def isTerminal(self):
        triples = [(0,1,2), (3,4,5), (6,7,8), (0,3,6), (1,4,7), (2,5,8), (0,4,8), (2,4,6)]
        for a,b,c in triples:
            if self.board[a] == self.board[b] == self.board[c] != 0:
                return True
        if len(filter(lambda x: x, self.board)) == 9:
            self.reward = 0
            return True # draw
        return False

    def getReward(self):
        if self.reward is not None:
            return self.reward
        return 1 if self.player == 2 else -1

    def getWinner(self):
        if self.getReward() == 0:
            return 0
        return 3 - self.player

    def doAction(self, action):
        copy = list(self.board)
        copy[action] = self.player
        return TicTacToeState(player=3-self.player, board=copy)

    def __str__(self):
        to_print = []
        s = "%s|%s|%s"
        def c(x):
            if x == 0:
                return ' '
            if x == 1:
                return 'X'
            if x == 2:
                return 'O'
        for x in xrange(3):
            to_print.append(s % (c(self.board[3*x]), c(self.board[3*x+1]), c(self.board[3*x+2])))
        return "\n".join(to_print)


# TODO Otheelo state?
