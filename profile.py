import cProfile, pstats
from montecarlo import UCT
from gnugolibs.go import GoState

g = GoState()

cProfile.runctx("UCT(g,5000)", globals(), locals(), "Go.prof")
s = pstats.Stats("Go.prof")
s.strip_dirs().sort_stats("time").print_stats()
