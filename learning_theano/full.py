import numpy
import theano
import theano.tensor as T

class FullyConnectedLayer(object):
    def __init__(self, inp, n_in, n_out, prev_layer=None):
        """
        A standard fully connected hidden layer.
        Uses sigmoid as the activation function (may change later?)

        input = theano matrix, shape (n_samples, n_in)
        """
        rng = numpy.random.RandomState()
        self.n_in = n_in
        self.n_out = n_out
        if prev_layer is not None:
            assert prev_layer.n_out == n_in, 'Previous layer has %d outputs and this layer expectes %d inputs' % (prev_layer.n_out, n_in)

        self.input = inp
        # initialize weights randomly
        W_values = numpy.asarray(
            rng.uniform(
                low=-4 *numpy.sqrt(6. / (n_in + n_out)),
                high=4 * numpy.sqrt(6. / (n_in + n_out)),
                size=(n_in, n_out),
            ),
            dtype=theano.config.floatX
        )
        self.W = theano.shared(value=W_values, name='W', borrow=True)

        # initialize bias to 0
        b_values = numpy.zeros( (n_out,), dtype=theano.config.floatX)
        self.b = theano.shared(value=b_values, name='b', borrow=True)

        lin_output = T.dot(self.input, self.W) + self.b

        # final outputs
        self.output = T.nnet.sigmoid(lin_output)
        # propagate the params up through the levels of the neural net

        # storing this separately makes regularization easier
        self.only_weight_params = [self.W]
        self.params = [self.W, self.b]

        if prev_layer is not None:
            self.only_weight_params += prev_layer.only_weight_params
            self.params += prev_layer.params

class SoftmaxLayer(object):
    def __init__(self, inp, n_in, n_out, prev_layer=None):
        """
        The final layer of the neural net.
        Does a softmax to get a probability distribution
        """
        self.n_in = n_in
        self.n_out = n_out
        if prev_layer is not None:
            assert prev_layer.n_out == n_in, 'Previous layer has %d outputs and this layer expectes %d inputs' % (prev_layer.n_out, n_in)

        # initialize all params to 0
        self.W = theano.shared(
            value=numpy.zeros(
                (n_in, n_out),
                dtype=theano.config.floatX,
            ),
            name='W',
            borrow=True,
        )

        self.b = theano.shared(
            value=numpy.zeros(
                (n_out,),
                dtype=theano.config.floatX,
            ),
            name='b',
            borrow=True,
        )

        # softmax of input to get distribution over moves
        self.lin_output = T.dot(inp, self.W) + self.b
        self.p_y_given_x = T.nnet.softmax(self.lin_output)
        # most likely class
        self.y_pred = T.argmax(self.p_y_given_x, axis=1)

        self.only_weight_params = [self.W]
        self.params = [self.W, self.b]
        if prev_layer is not None:
            self.only_weight_params += prev_layer.only_weight_params
            self.params += prev_layer.params

    def neg_log_likelihood(self, y):
        # assumes classes are integers, n_out different classes
        # y is vector of examples and classes
        return -T.mean(T.log(self.p_y_given_x)[T.arange(y.shape[0]), y])

    def squared_diff(self, y):
        # y and lin_output have shape (batch_size, n_out)
        squared_diffs = T.pow(self.lin_output - y, 2)
        # sum the squared difference in each component
        norms = T.sum(squared_diffs, axis=1)
        # find mean norm over each example in batch
        return T.mean(norms)

    def errors(self, y):
        return T.mean(T.neq(self.y_pred, y))

    def kl_divergence(self, true_dists):
        # Returns the KL divergence D(P || Q), where the given argument is P
        # and the softmax from the neural net is Q

        # Expected input:
        # true_dists.shape = (batch_size, n_out),
        # For each sample in the batch, \sum_i true_dist(i) = 1

        # Sometimes the true distribution is 0
        # (this happens if an action is not legal in the current state.)
        # in this scenario, we need to treat 0 log 0 as 0.

        # NONE OF THE BELOW COMMENTS ARE RELEVANT
        # They came out of a misunderstanding of what was getting differentiated
        # leaving for posterity (just in case I need to differentiate by 
        # distribution P instead of distribution Q)

        # Additionally, we need to make sure the gradient at 0 is some large negative
        # number (the limit as true_dist -> 0 of the gradient is -inf.)
        # By using this trick, T.grad gives the desired behavior
        #
        # (Theano's gradient finder will differentiate piecewise functions correctly,
        # so we need the function at x = 0 to be linear, constant * x. This gives a constant,
        # negative derivative when T.grad looks up the piece of the function to differentiate
        # at x == 0)

        kl = T.switch(
                T.eq(true_dists, 0),
                0,
                true_dists * (T.log(true_dists) - T.log(self.p_y_given_x)),
             )

        kl_divergences = T.sum(kl, axis=1)
        # average over entire batch
        return T.mean(kl_divergences)

    def total_variation(self, true_dists):
        # true_dists.shape = (batch_size, n_out)
        tv = T.sum(T.abs(self.p_y_given_x, true_dists), axis=1)
        return T.mean(tv)
