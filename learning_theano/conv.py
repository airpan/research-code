import theano
import theano.tensor as T
from theano.tensor.nnet import conv
from theano.tensor.signal import downsample
import numpy

class SimpleConvNetLayer(object):
    # no max pooling
    def __init__(self,
                 inp,
                 n_out_filters, # filters to output
                 n_input_feature_maps, # feature maps coming in
                 filter_dim, # size of outputted feature maps (height x width)
                 image_shape, # a tuple of (batch_size, n_input_feature_maps, incoming_image_height, incoming_image_width)
                 pool_size, # the max pooling size, (#rows, #cols)
                 prev_layer=None):
        # image_shape = (batch_size, num input feature maps, img height, img width)
        # input = theano function
        # n_out_filters = number of output filters
        assert image_shape[1] == n_input_feature_maps
        rng = numpy.random.RandomState()
        self.input = inp

        # number of inputs to each hidden unit
        # num input feature maps * filter size
        fan_in = n_input_feature_maps * numpy.prod(filter_dim)

        # number of outputs
        # output feature maps * filter size / pool size (no pooling gives pool size 1)
        fan_out = (n_out_filters * numpy.prod(filter_dim)) / numpy.prod(pool_size)

        # Some validity checking between conv layer and hidden layer
        out_shape = (image_shape[2] - filter_dim[0] + 1, image_shape[3] - filter_dim[1] + 1)
        self.n_out = (n_out_filters * numpy.prod(out_shape)) / numpy.prod(pool_size)

        # magic bound?
        w_bound = numpy.sqrt(6. / (fan_in + fan_out) )

        # assumes filter_dim is a regular tuple, not a numpy array
        weight_shape = (n_out_filters, n_input_feature_maps) + filter_dim

        # initialize weight matrix
        self.W = theano.shared(
            numpy.asarray(
                rng.uniform(low=-w_bound, high=w_bound, size=weight_shape),
                dtype=theano.config.floatX
            ),
            borrow=True,
        )

        # bias initialization. 1D tensor, one bias per output feature
        b_values = numpy.zeros( (n_out_filters,), dtype=theano.config.floatX)
        self.b = theano.shared(value=b_values, borrow=True)

        conv_out = conv.conv2d(
            input=inp,
            filters=self.W,
            filter_shape=weight_shape,
            image_shape=image_shape,
        )

        # do max pooling
        pooled_out = downsample.max_pool_2d(
            input=conv_out,
            ds=pool_size,
            ignore_border=False, # TODO false or true here?
        )

        # add bias to convolutional output
        self.output = T.nnet.sigmoid(
            pooled_out + self.b.dimshuffle('x', 0, 'x', 'x')
        )

        # Need the flattened output for the fully connected
        # layer at the end
        self.flattened_output = self.output.flatten(2)

        # stored params to allow for updates
        self.only_weight_params = [self.W]
        self.params = [self.W, self.b]

        if prev_layer is not None:
            self.only_weight_params += prev_layer.only_weight_params
            self.params += prev_layer.params

        # generally, the convolutional layer is not the final layer of the net,
        # so there is not cost function for this class (rely on the cost function
        # elsewhere
