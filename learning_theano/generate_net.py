import theano
import theano.tensor as T
import numpy
from conv import SimpleConvNetLayer
from full import FullyConnectedLayer, SoftmaxLayer

import time
# enable import from parent directory
"""
import os,sys
sys.path.insert(1, os.path.join(sys.path[0], '..'))
import cfour
"""
import cPickle

class MultiLayerNet(object):
    """
    Represents the structure of the neural net.
    The structure is defined as a list of layers, where
        each layer has a list of parameters
        for how to reconstruct it.
    The goal is that we can define how the net is
    constructed, then have the net loader use that
    description
    """
    CONV = 'conv_layer'
    FULLY_CONNECTED = 'full'
    SOFTMAX = 'softmax'
    # Python workaround for a switch statement
    NET_CLASS_MAP = {
        CONV: SimpleConvNetLayer,
        FULLY_CONNECTED: FullyConnectedLayer,
        SOFTMAX: SoftmaxLayer,
    }


    def __init__(self, inp, types, kwargs_list, init_weight_file=None):
        """
        inp: Theano input vector/matrix to this net
        types: List of the types of NN layers used
        args: List of argument tuples used in NN construction
        init_weight_file: Name of file which contains trained params
        """
        assert len(types) == len(kwargs_list)
        self.layers = []

        for layer_type, kwargs in zip(types, kwargs_list):
            if self.layers == []:
                layer = MultiLayerNet.NET_CLASS_MAP[layer_type](
                    inp=inp,
                    **kwargs
                )
            else:
                if prev_type == MultiLayerNet.CONV and \
                   layer_type == MultiLayerNet.FULLY_CONNECTED:
                    next_input = self.layers[-1].flattened_output
                else:
                    next_input = self.layers[-1].output
                layer = MultiLayerNet.NET_CLASS_MAP[layer_type](
                    inp=next_input,
                    prev_layer=self.layers[-1],
                    **kwargs
                )
            self.layers.append(layer)
            prev_type = layer_type

        self.final_layer = self.layers[-1]
        if init_weight_file is not None:
            self.load_net(init_weight_file)

    def save_net(self, filename):
        with open(filename, 'wb') as dump_file:
            for t_variable in self.final_layer.params:
                cPickle.dump(t_variable.get_value(borrow=True), save_file, -1)

    def load_net(self, filename):
        with open(filename) as save_file:
            for t_variable in self.final_layer.params:
                t_variable.set_value(cPickle.load(save_file).astype(theano.config.floatX), borrow=True)


def load_data(file_path):
    with open(file_path) as f:
        mat = cPickle.load(f)

    # the data is formatted as a list of tuples
    # each tuple is ( [feature_values], goal_value )
    # by the end we want to split these into two matrices,
    # one feature matrix and one goal matrix

    train = mat['train']
    train_x = [tup[0] for tup in train]
    train_y = [tup[1] for tup in train]

    validate = mat['validate']
    validate_x = [tup[0] for tup in validate]
    validate_y = [tup[1] for tup in validate]

    test = mat['test']
    test_x = [tup[0] for tup in test]
    test_y = [tup[1] for tup in test]

    # For all of the following, we do not modify the values of the training/validation/test
    # sets after this (treat as read-only.) So, we can safely set borrow to True

    t_train_x = theano.shared(
        numpy.asarray(train_x, dtype=theano.config.floatX),
        borrow=True,
    )

    # need to fix shape of the goal values array
    t_train_y = theano.shared(
        numpy.asarray(train_y, dtype=theano.config.floatX).reshape( (-1, 1) ),
        borrow=True,
    )

    t_validate_x = theano.shared(
        numpy.asarray(validate_x, dtype=theano.config.floatX),
        borrow=True,
    )

    # need to fix shape of the goal values array
    t_validate_y = theano.shared(
        numpy.asarray(validate_y, dtype=theano.config.floatX).reshape( (-1, 1) ),
        borrow=True,
    )

    t_test_x = theano.shared(
        numpy.asarray(test_x, dtype=theano.config.floatX),
        borrow=True,
    )

    # need to fix shape of the goal values array
    t_test_y = theano.shared(
        numpy.asarray(test_y, dtype=theano.config.floatX).reshape( (-1, 1) ),
        borrow=True,
    )

    return {
        'train': (t_train_x, t_train_y),
        'validate': (t_validate_x, t_validate_y),
        'test': (t_test_x, t_test_y),
    }

def initialize_net(inp, n_in=42, n_hidden=50, n_out=1):
    """
    Creates a simple 1 hidden layer net.
    Returns a list of layers
    """
    # send to a hidden layer
    layer0 = FullyConnectedLayer(
        inp=inp,
        n_in=n_in,
        n_out=n_hidden, # 50 hidden nodes, arbitrary
    )

    # The training only uses the linear output for now
    # (It doesn't use a softmax because the training data I have is structured as
    # state, value pairs, instead of state, [q_values_for_each_action])
    final_layer = SoftmaxLayer(
        inp=layer0.output,
        n_in=n_hidden,
        n_out=n_out,
        prev_layer=layer0,
    )

    return [layer0, final_layer]

# Bad style to copy paste this much
def get_untrained_net(learn_rate=0.1, L1_reg=0., L2_reg=0.):
    # give an untrained net to the MCTS code
    x = T.matrix('x') # shape (n_samples, n_in_to_first_layer)
    y = T.matrix('y') # shape (n_samples, n_out_from_last_layer)
    layer0, final_layer = initialize_net(x)

    # find the gradients of the cost function
    cost = final_layer.squared_diff(y) + \
            L1_reg * (abs(final_layer.W).sum() + abs(layer0.W).sum()) + \
            L2_reg * ((final_layer.W ** 2).sum() + (layer0.W ** 2).sum())

    gparams = [T.grad(cost, param) for param in final_layer.params]

    updates = [
        (param, param - learn_rate * gparam) for param, gparam in zip(final_layer.params, gparams)
    ]

    # finds cost on the training set and does SGD
    train_model = theano.function(
        inputs=[x, y],
        outputs=cost,
        updates=updates,
    )

    # we also need to see a value function for the MCTS code
    val_func = theano.function(
        inputs=[x],
        outputs=final_layer.lin_output,
    )

    # function takes a matrix, so this bundles 1D feature vector into matrix with 1 row
    def wrapped_val_func(feat):
        return val_func([feat])

    return train_model, wrapped_val_func, final_layer.params


# This is another copy-pasted method
# Fitting this into the code above feels messier than doing this
# (not only is the cost function different, but also we need
# to output something a bit different, and at some point
# may need to manually hack the gradient.)
def get_kl_net_to_train(learn_rate=0.1, L1_reg=0., L2_reg=0.):
    x = T.matrix('x') # shape (n_samples, n_in_to_first_layer)
    y = T.matrix('y') # shape (n_samples, n_out_from_last_layer)
    layer0, final_layer = initialize_net(x, n_out=7)

    # TODO do regularization or not?
    cost = final_layer.kl_divergence(y) + \
            L1_reg * (abs(final_layer.W).sum() + abs(layer0.W).sum()) + \
            L2_reg * ((final_layer.W ** 2).sum() + (layer0.W ** 2).sum())

    gparams = [T.grad(cost, param) for param in final_layer.params]

    updates = [
        (param, param - learn_rate * gparam) for param, gparam in zip(final_layer.params, gparams)
    ]

    # finds cost on the training set and does SGD
    train_model = theano.function(
        inputs=[x, y],
        outputs=cost,
        updates=updates,
    )

    # find policy this neural net recommends
    policy = theano.function(
        inputs=[x],
        outputs=final_layer.p_y_given_x,
    )

    return train_model, policy, final_layer.params


def train_net(learn_rate=0.1, L1_reg=0., L2_reg=0., batch_size=10):
    """
    Creates a simple 1 hidden layer neural net and fits it to training data.
    """
    print 'Loading the data'
    data = load_data("cfour_data")
    train_x, train_y = data['train']
    validate_x, validate_y = data['validate']
    test_x, test_y = data['test']

    print 'Building the model'
    # Get a simple one layer net working

    x = T.matrix('x') # shape (n_samples, n_in_to_first_layer)
    y = T.matrix('y') # shape (n_samples, n_out_from_last_layer)
    index = T.lscalar() # index for a minibatch
    n_train_batches = train_y.get_value().shape[0] // batch_size
    n_valid_batches = validate_y.get_value().shape[0] // batch_size
    n_test_batches = test_y.get_value().shape[0] // batch_size

    layer0, final_layer = initialize_net(x)

    # lets us inspect the output on the training set
    peek = theano.function(
        inputs=[index],
        outputs=final_layer.lin_output,
        givens={
            x: train_x[index * batch_size:(index+1) * batch_size],
        }
    )

    # tests model on batch of validation set
    validate_model = theano.function(
        inputs=[index],
        outputs=final_layer.squared_diff(y),
        givens={
            x: validate_x[index * batch_size:(index+1) * batch_size],
            y: validate_y[index * batch_size:(index+1) * batch_size],
        }
    )

    # tests model on a batch of the test set
    test_model = theano.function(
        inputs=[index],
        outputs=final_layer.squared_diff(y),
        givens={
            x: test_x[index * batch_size:(index+1) * batch_size],
            y: test_y[index * batch_size:(index+1) * batch_size],
        }
    )

    # find the gradients of the cost function
    cost = final_layer.squared_diff(y) + \
            L1_reg * (abs(final_layer.W).sum() + abs(layer0.W).sum()) + \
            L2_reg * ((final_layer.W ** 2).sum() + (layer0.W ** 2).sum())
    gparams = [T.grad(cost, param) for param in final_layer.params]

    updates = [
        (param, param - learn_rate * gparam) for param, gparam in zip(final_layer.params, gparams)
    ]

    # finds cost on the training set and does SGD
    train_model = theano.function(
        inputs=[index],
        outputs=cost,
        updates=updates,
        givens={
            x: train_x[index * batch_size:(index+1) * batch_size],
            y: train_y[index * batch_size:(index+1) * batch_size],
        }
    )

    print 'Training the model'
    done_training = False
    epoch = 0
    # patience is measured in epochs
    patience = 2
    epochs_since_last_improvement = 0
    significant_threshold = 0.99

    iternum = 0
    best_validation = numpy.inf
    start_time = time.time()
    while not done_training:
        epoch += 1
        for batch_index in xrange(n_train_batches):
            batch_avg_cost = train_model(batch_index)
            iternum += 1
        # validate at the end of every run over the dataset
        validation_losses = [validate_model(i) for i in xrange(n_valid_batches)]
        epoch_validation_loss = numpy.mean(validation_losses)
        print(
            "epoch %i, validation error %f" % (epoch, epoch_validation_loss)
        )
        if epoch_validation_loss < best_validation:
            # increase patience on significant gains
            if epoch_validation_loss < best_validation * significant_threshold:
                print "Significant improvement, increasing patience"
                patience += 1
            best_validation = epoch_validation_loss

            # try on the test set
            test_losses = [test_model(i) for i in xrange(n_test_batches)]
            test_score = numpy.mean(test_losses)
            print(
                "epoch %i, found better model, has test error %f" % (epoch, test_score)
            )
            epochs_since_last_improvement = 0
        else:
            epochs_since_last_improvement += 1
            if epochs_since_last_improvement > patience:
                done_training = True

    print 'Training time: %f seconds' % (time.time() - start_time)

    dump_start = time.time()
    print 'Training complete, dumping parameters'
    ########
    # To reload the net, you must know the original structure of the network!
    ########
    with open('net_params', 'wb') as save_file:
        for t_variable in final_layer.params:
            cPickle.dump(t_variable.get_value(borrow=True), save_file, -1)

    print 'Model saving time: %f seconds' % (time.time() - dump_start)
    return {'peek': peek, 'test_model': test_model, 'train_model': train_model, 'validate_model': validate_model}


def load_net(params_file_path, n_in=42, n_hidden=50, n_out=7, mat_input=False):
    """
    TENTAIVELY DEPRECATED, USE THE GENERAL LOADING METHOD
    """
    # load from a cPickle dump, return layers in order of use
    # TODO make a class that encodes neural net structure so it can be passed around
    x = T.vector('x') if not mat_input else T.matrix('x')
    # send to a hidden layer
    layer0 = FullyConnectedLayer(
        inp=x,
        n_in=n_in,
        n_out=n_hidden,
    )

    # The training only uses the linear output for now
    # (It doesn't use a softmax because the training data I have is structured as
    # state, value pairs, instead of state, [q_values_for_each_action])
    final_layer = SoftmaxLayer(
        inp=layer0.output,
        n_in=n_hidden,
        n_out=n_out,
        prev_layer=layer0,
    )

    with open(params_file_path) as save_file:
        final_layer.W.set_value(cPickle.load(save_file), borrow=True)
        final_layer.b.set_value(cPickle.load(save_file), borrow=True)
        layer0.W.set_value(cPickle.load(save_file), borrow=True)
        layer0.b.set_value(cPickle.load(save_file), borrow=True)

    return {'args': [x], 'layers': [layer0, final_layer]}

def load_kl_net(params_file_path):
    x = T.matrix('x')
    types = [MultiLayerNet.FULLY_CONNECTED, MultiLayerNet.SOFTMAX]
    args = [
        {'n_in':42, 'n_out':50},
        {'n_in':50, 'n_out':7},
    ]
    mn = MultiLayerNet(inp=x, types=types, kwargs_list=args, init_weight_file=params_file_path)
    sto_policy = theano.function(
        inputs=[x],
        outputs=mn.final_layer.p_y_given_x,
    )

    return sto_policy


# the quick hack to set up testing
# loads the net, returns a theano function that gives values for a given state
def create_net_value_function(file_path):
    info = load_net(file_path, n_out=1, mat_input=False)
    x = info['args'][0]
    layer0, final_layer = info['layers']

    val_func = theano.function(
        inputs=[x],
        outputs=final_layer.lin_output,
    )
    return val_func

def load_mnist():
    print '... loading data'

    # Load the dataset
    with open('mnist.pkl') as f:
        train_set, valid_set, test_set = cPickle.load(f)
    #train_set, valid_set, test_set format: tuple(input, target)
    #input is an numpy.ndarray of 2 dimensions (a matrix)
    #witch row's correspond to an example. target is a
    #numpy.ndarray of 1 dimensions (vector)) that have the same length as
    #the number of rows in the input. It should give the target
    #target to the example with the same index in the input.

    def shared_dataset(data_xy, borrow=True):
        """ Function that loads the dataset into shared variables

        The reason we store our dataset in shared variables is to allow
        Theano to copy it into the GPU memory (when code is run on GPU).
        Since copying data into the GPU is slow, copying a minibatch everytime
        is needed (the default behaviour if the data is not in a shared
        variable) would lead to a large decrease in performance.
        """
        data_x, data_y = data_xy
        shared_x = theano.shared(numpy.asarray(data_x,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        shared_y = theano.shared(numpy.asarray(data_y,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        # When storing data on the GPU it has to be stored as floats
        # therefore we will store the labels as ``floatX`` as well
        # (``shared_y`` does exactly that). But during our computations
        # we need them as ints (we use labels as index, and if they are
        # floats it doesn't make sense) therefore instead of returning
        # ``shared_y`` we will have to cast it to int. This little hack
        # lets ous get around this issue
        return shared_x, T.cast(shared_y, 'int32')

    test_set_x, test_set_y = shared_dataset(test_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    train_set_x, train_set_y = shared_dataset(train_set)

    rval = [(train_set_x, train_set_y), (valid_set_x, valid_set_y),
            (test_set_x, test_set_y)]
    return rval

if __name__ == '__main__':
    #model_functions = train_net()
    x = T.matrix('x')
    y = T.ivector('y')
    index = T.lscalar()
    batch_size = 500
    learning_rate = 0.1
    n_epochs = 200

    # Reshape input to be compatible
    inp = x.reshape( (batch_size, 1, 28, 28) )
    # Try it out on MNIST
    types = [MultiLayerNet.CONV, MultiLayerNet.CONV, MultiLayerNet.FULLY_CONNECTED, MultiLayerNet.SOFTMAX]
    args = [
        {'n_out_filters': 20, 'n_input_feature_maps': 1, 'filter_dim': (5, 5),
         'image_shape': (batch_size, 1, 28, 28), 'pool_size': (2,2)},
        # after convolution and max pooling, size is 12 x 12 for MNIST
        {'n_out_filters': 50, 'n_input_feature_maps': 20, 'filter_dim': (5, 5),
         'image_shape': (batch_size, 20, 12, 12), 'pool_size': (2,2)},
        {'n_in': 50 * 4 * 4, 'n_out': batch_size},
        {'n_in': batch_size, 'n_out': 10},
    ]
    data = load_mnist()
    train_set_x, train_set_y = data[0]
    valid_set_x, valid_set_y = data[1]
    test_set_x, test_set_y = data[2]
    n_train_batches = train_set_x.get_value(borrow=True).shape[0]
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
    n_test_batches = test_set_x.get_value(borrow=True).shape[0]
    n_train_batches //= batch_size
    n_valid_batches //= batch_size
    n_test_batches //= batch_size

    mn = MultiLayerNet(inp=inp, types=types, kwargs_list=args)
    """
    fn = theano.function(
        inputs=[x],
        outputs=mn.final_layer.p_y_given_x
    )
    """
    # all below is copy pasted
    test_model = theano.function(
        [index],
        mn.final_layer.errors(y),
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size],
            y: test_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    validate_model = theano.function(
        [index],
        mn.final_layer.errors(y),
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            y: valid_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    cost = mn.final_layer.neg_log_likelihood(y)

    # create a list of all model parameters to be fit by gradient descent
    params = mn.final_layer.params

    # create a list of gradients for all model parameters
    grads = T.grad(cost, params)

    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    updates = [
        (param_i, param_i - learning_rate * grad_i)
        for param_i, grad_i in zip(params, grads)
    ]

    train_model = theano.function(
        [index],
        cost,
        updates=updates,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size],
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )
    # end-snippet-1

    ###############
    # TRAIN MODEL #
    ###############
    print '... training'
    # early-stopping parameters
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    validation_frequency = min(n_train_batches, patience / 2)
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = time.clock()

    epoch = 0
    done_looping = False

    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        for minibatch_index in xrange(n_train_batches):

            iter = (epoch - 1) * n_train_batches + minibatch_index

            if iter % 100 == 0:
                print 'training @ iter = ', iter
            cost_ij = train_model(minibatch_index)

            if (iter + 1) % validation_frequency == 0:

                # compute zero-one loss on validation set
                validation_losses = [validate_model(i) for i
                                     in xrange(n_valid_batches)]
                this_validation_loss = numpy.mean(validation_losses)
                print('epoch %i, minibatch %i/%i, validation error %f %%' %
                      (epoch, minibatch_index + 1, n_train_batches,
                       this_validation_loss * 100.))

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:

                    #improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)

                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter

                    # test it on the test set
                    test_losses = [
                        test_model(i)
                        for i in xrange(n_test_batches)
                    ]
                    test_score = numpy.mean(test_losses)
                    print(('     epoch %i, minibatch %i/%i, test error of '
                           'best model %f %%') %
                          (epoch, minibatch_index + 1, n_train_batches,
                           test_score * 100.))

            if patience <= iter:
                done_looping = True
                break

    end_time = time.clock()
    print('Optimization complete.')
    print('Best validation score of %f %% obtained at iteration %i, '
          'with test performance %f %%' %
          (best_validation_loss * 100., best_iter + 1, test_score * 100.))
    print >> sys.stderr, ('The code for file ' +
                          os.path.split(__file__)[1] +
                          ' ran for %.2fm' % ((end_time - start_time) / 60.)) 
