import theano
import theano.tensor as T

x = T.vector()
y = T.vector()
kl = T.switch(
    T.eq(x, 0),
    -500 * x,
    x * (T.log(x) - T.log(y)),
)

kl_sum = T.sum(kl)

f1 = theano.function([x,y], kl)
f2 = theano.function([x,y], kl_sum)

gy = theano.function([x,y], T.grad(kl_sum, wrt=y))
gx = theano.function([x,y], T.grad(kl_sum, wrt=x))
