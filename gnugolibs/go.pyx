# cython: profile=True

# In Cython there isn't a way to specify it differently
# at compile time, so we assume float32 instead of
# looking up the theano config
cimport go
cimport cython
import numpy as np # required for import below
cimport numpy as np
import theano
DTYPE = np.float32

ctypedef np.float32_t DTYPE_t

import time
import pdb

DEF PASS = 0
DEF EMPTY = 0
DEF WHITE = 1
DEF BLACK = 2
DEF DEFAULT_MEMORY = 8 # number of megabytes the cache can use
DEF BOARDSIZE = ((19+2) * (19+1) + 1)

cdef int INITTED = 0
DEF MAX_GNUGO_HISTORY = 1000
# GNUGo library caps out at above
# Doing more than MAX_HISTORY moves from the start state
# makes undo fail, so we set some hopefully large enough
# constant to make sure that cap never gets hit
DEF MAX_MOVES = 750


# WHENEVER GETTING A POSITION IN THE RAW BOARD,
# TRANSFORM WITH (i,j) => 21 + 20 * i + j
# and transform (i * size + j) => (i,j)
# It didn't seem like Cython supported macros,
# so I hacked something together
# (the function call overhead added up to a lot, ~25% of
# the run time, so the terrible maintenance is worth it.)

cdef class GoState:
    cdef public int player
    cdef public int reward
    cdef int npasses
    cdef int size
    cdef int history[MAX_MOVES]
    cdef int settled[BOARDSIZE]
    cdef int nmoves

    def __init__(self, int player=WHITE, int size=19):
        """
        Player = 1 or 2
        reward = +1 for player 1 win, -1 for player 2 win, 0 for a draw
        """
        global INITTED
        # these are set by the __init__ call above, but make it more explicit
        self.player = player
        self.npasses = 0
        self.nmoves = 0
        self.size = size
        if not INITTED:
            go.init_gnugo(DEFAULT_MEMORY, time.time())
            INITTED = 1
        go.gnugo_clear_board(size)

    @cython.boundscheck(False)
    def doAction(self, int loc):
        """ Assumes that action is legal """
        if loc == self.size * self.size:
            loc = PASS
        else:
            loc = 21 + (loc // self.size * 20) + loc % self.size
        if go.is_pass(loc):
            self.npasses += 1
        else:
            self.npasses = 0
        # 1 == white, 2 == black. I think the official rule is the other way
        # around but oh well
        #go.trymove(loc, self.player, '')
        go.gnugo_play_move(loc, self.player)
        self.player = WHITE + BLACK - self.player
        self.history[self.nmoves] = loc
        self.nmoves += 1
        return self

    @cython.boundscheck(False)
    def reverseAction(self):
        if not go.undo_move(1):
            print 'Undo move failed!'
        self.nmoves -= 1
        self.player = WHITE + BLACK - self.player
        if self.nmoves > 0 and go.is_pass(self.history[self.nmoves-1]):
            self.npasses = 1
        else:
            self.npasses = 0
        return self

    @cython.boundscheck(False)
    def reverseActions(self, int n):
        if not go.undo_move(n):
            print 'Undoing %d moves failed!' % n
        self.nmoves -= n
        if n % 2 == 1:
            self.player = (WHITE + BLACK) - self.player
        if self.nmoves > 0 and go.is_pass(self.history[self.nmoves-1]):
            self.npasses = 1
        else:
            self.npasses = 0
        return self

    cpdef list getActions(self):
        act = []
        for a in xrange(self.size * self.size):
            if self.isLegal(a):
                act.append(a)
        act.append(self.size * self.size)
        return act

    cpdef int getRandomAction(self):
        return np.random.choice(self.getActions())

    # Allows self capture
    cpdef int isLegal(self, int action):
        if action == self.size * self.size:
            # pass is always legal
            return True
        else:
            action = 21 + (action // self.size * 20) + action % self.size
        return go.is_legal(action, self.player)

    def isTerminal(self):
        return self.npasses == 2 or self.nmoves >= MAX_MOVES

    def getReward(self):
        cdef int sc = self._score()
        if sc == 0:
            self.reward = 0
        elif sc > 0:
            self.reward = 1
        else:
            self.reward = -1
        return self.reward

    @cython.boundscheck(False)
    cdef int _get_region(self, int row, int col):
        # Computes the empty region that contains (row, col)
        # Updates the settled array appropriately.
        # Returns the score from whites perspective
        # (a group for white is positive. A group for black
        # is negative. A group surrounded by both is neutral)
        cdef int size = 0
        cdef int white_border = 0
        cdef int black_border = 0
        cdef int index

        to_check = []
        to_check.append((row, col))
        self.settled[21 + 20 * row + col] = 1
        size = 1
        while to_check:
            r, c = to_check.pop()
            for nei in ( (r-1,c), (r+1,c), (r,c-1), (r,c+1) ):
                r1, c1 = nei
                if r1 < 0 or r1 >= self.size or c1 < 0 or c1 >= self.size:
                    continue
                index = 21 + 20 * r1 + c1
                if go.board[index] == EMPTY and not self.settled[index]:
                    size += 1
                    self.settled[index] = 1
                    to_check.append((r1, c1))
                elif go.board[index] == WHITE:
                    white_border = 1
                elif go.board[index] == BLACK:
                    black_border = 1
        return white_border * size - black_border * size

    @cython.boundscheck(False)
    cdef int _score(self):
        cdef int i
        cdef int j
        cdef int index
        cdef int score = 0
        # reset marker used for DFS
        for i in xrange(BOARDSIZE):
            self.settled[i] = 0

        for i in xrange(self.size):
            for j in xrange(self.size):
                index = 21 + 20 * i + j
                if self.settled[index]:
                    continue
                if go.board[index] == EMPTY:
                    score += self._get_region(i, j)
                elif go.board[index] == WHITE:
                    score += 1
                elif go.board[index] == BLACK:
                    score -= 1
        return score

    def getWinner(self):
        if self.reward == 1:
            return 1
        elif self.reward == -1:
            return 2
        elif self.reward == 0:
            return 0
        else:
            raise ValueError("getWinner() called before reward was set")

    @cython.boundscheck(False)
    def toFeatures(self):
        cdef np.ndarray[DTYPE_t, ndim=3] bitmaps = np.zeros( (2, self.size, self.size), dtype=DTYPE )
        cdef int x, y, index
        for x in xrange(self.size):
            for y in xrange(self.size):
                index = 21 + 20 * x + y
                if go.board[index] == WHITE:
                    bitmaps[0][x][y] = 1.0
                elif go.board[index] == BLACK:
                    bitmaps[1][x][y] = 1.0
        return bitmaps

    @cython.boundscheck(False)
    def toFlippedFeatures(self):
        cdef np.ndarray[DTYPE_t, ndim=3] bitmaps = np.zeros( (2, self.size, self.size), dtype=DTYPE )
        cdef int x, y, index
        for x in xrange(self.size):
            for y in xrange(self.size):
                index = 21 + 20 * x + y
                if go.board[index] == WHITE:
                    bitmaps[1][x][y] = 1.0
                elif go.board[index] == BLACK:
                    bitmaps[0][x][y] = 1.0
        return bitmaps

    def __str__(self):
        """
        header = ['  ']
        for i in xrange(self.size):
            header.append('%3d' % i)
        header = ''.join(header)
        to_print = [header]
        for j in xrange(self.size):
            row = [self.board.get(i,j) for i in xrange(self.size)]
            row_st = []
            row_st.append('%3d' % j)
            row_st.extend('|.|' if c is None else '|B|' if c == 'b' else '|W|' for c in row)
            to_print.append(''.join(row_st))
        return '\n'.join(to_print)
        """
        self.view()
        return ''

    def view(self):
        go.showboard(0)

