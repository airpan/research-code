# the following are from board.h
# the board libraries are provided in the .a library files
# I don't believe Cython supports that syntax, so these are extern
# with no specifier and the implmentations are hooked in
# by the Makefile
# The .h files in this folder are for reference
cdef extern:
    # from board.h
    ctypedef unsigned char Intersection
    # defines are not exposed to cython, recopy it here
    DEF BOARDSIZE = ((19+2) * (19+1) + 1)
    Intersection board[BOARDSIZE]

    void clear_board()
    void add_stone(int pos, int color)
    void remove_stone(int pos)
    # Play/undo a permanent move
    # These actions are more costly. Try to use them
    # only when a move needs to be permanantly committed
    # (The scoring routine requires all actions to be committed,
    # so use it then.)

#    void play_move(int pos, int color)
    void gnugo_play_move(int move, int color)
    void gnugo_clear_board(int boardsize)
    int undo_move(int n)

    # Play/undo a temporary move
    # These are much cheaper actions
    int trymove(int pos, int color, const char *message)
    void popgo()

    int is_pass(int pos)
    int is_legal(int pos, int color)
    int is_suicide(int pos, int color)
    int is_illegal_ko_capture(int pos, int color)
    int is_allowed_move(int pos, int color)
    int is_ko(int pos, int color, int *ko_pos)
    int is_ko_point(int pos)
    int does_capture_something(int pos, int color)
    int is_self_atari(int pos, int color)

    # from showbord.c
    void showboard(int xo) # should always be 0 (colored printing not supported)

    # added to boardlib.c
    void set_board_size(int size) # sets the board size, then clears it

    # from gnugo.h
    float gnugo_estimate_score(float *upper, float *lower)
    void reset_engine()
    float who_wins_value(int color)

    # from interface.c
    void init_gnugo(float memory, unsigned int random_seed)

