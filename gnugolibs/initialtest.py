import go

def random_game():
    g = go.GoState()
    n = 0
    while not g.isTerminal():
        g.view()
        act = g.getRandomAction()
        print "Doing %d" % act
        g.doAction(act)
        n += 1
    g.getReward()
    for _ in xrange(n):
        g.reverseAction()

lines = open("broke").readlines()
lines = [line.strip().split()[1:-1] for line in lines]

g = go.GoState()

for y in xrange(9):
    for x in xrange(9):
        if lines[y][x] == 'X':
            g.doAction(81)
            g.doAction(x + y * 9)

for y in xrange(9):
    for x in xrange(9):
        if lines[y][x] == 'O':
            g.doAction(x + y * 9)
            g.doAction(81)

