<html lang="en">
<head>
<title>Incremental Board - GNU Go Documentation</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name="description" content="GNU Go Documentation">
<meta name="generator" content="makeinfo 4.13">
<link title="Top" rel="start" href="index.html#Top">
<link rel="up" href="Libboard.html#Libboard" title="Libboard">
<link rel="prev" href="The-Board-Array.html#The-Board-Array" title="The Board Array">
<link rel="next" href="Some-Board-Functions.html#Some-Board-Functions" title="Some Board Functions">
<link href="http://www.gnu.org/software/texinfo/" rel="generator-home" title="Texinfo Homepage">
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css"><!--
  pre.display { font-family:inherit }
  pre.format  { font-family:inherit }
  pre.smalldisplay { font-family:inherit; font-size:smaller }
  pre.smallformat  { font-family:inherit; font-size:smaller }
  pre.smallexample { font-size:smaller }
  pre.smalllisp    { font-size:smaller }
  span.sc    { font-variant:small-caps }
  span.roman { font-family:serif; font-weight:normal; } 
  span.sansserif { font-family:sans-serif; font-weight:normal; } 
--></style>
</head>
<body>
<div class="node">
<a name="Incremental-Board"></a>
<p>
Next:&nbsp;<a rel="next" accesskey="n" href="Some-Board-Functions.html#Some-Board-Functions">Some Board Functions</a>,
Previous:&nbsp;<a rel="previous" accesskey="p" href="The-Board-Array.html#The-Board-Array">The Board Array</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="Libboard.html#Libboard">Libboard</a>
<hr>
</div>

<h3 class="section">15.3 Incremental Board data structures</h3>

<p>In addition to the global board state, the algorithms in <samp><span class="file">board.c</span></samp>
implement a method of incremental updates that keeps track of the
following information for each string:

     <ul>
<li>The color of the string. 
<li>Number of stones in the string. 
<li>Origin of the string, i.e. a canonical reference point, defined
to be the stone with smallest 1D board coordinate. 
<li>A list of the stones in the string. 
<li>Number of liberties. 
<li>A list of the liberties. If there are too many liberties the list is
truncated. 
<li>The number of neighbor strings. 
<li>A list of the neighbor strings. 
</ul>

   <p>The basic data structure is

<pre class="example">     struct string_data {
       int color;                       /* Color of string, BLACK or WHITE */
       int size;                        /* Number of stones in string. */
       int origin;                      /* Coordinates of "origin", i.e. */
                                        /* "upper left" stone. */
       int liberties;                   /* Number of liberties. */
       int libs[MAX_LIBERTIES];         /* Coordinates of liberties. */
       int neighbors;                   /* Number of neighbor strings */
       int neighborlist[MAXCHAIN];      /* List of neighbor string numbers. */
       int mark;                        /* General purpose mark. */
     };
     
     struct string_data string[MAX_STRINGS];
</pre>
   <p>It should be clear that almost all information is stored in the
<code>string</code> array. To get a mapping from the board coordinates to the
<code>string</code> array we have

<pre class="example">     static int string_number[BOARDMAX];
</pre>
   <p class="noindent">which contains indices into the <code>string</code> array. This information is only
valid at nonempty vertices, however, so it is necessary to first
verify that <code>board[pos] != EMPTY</code>.

   <p>The <code>string_data</code> structure does not include an array of the stone
coordinates. This information is stored in a separate array:

<pre class="example">     static int next_stone[BOARDMAX];
</pre>
   <p>This array implements cyclic linked lists of stones. Each vertex
contains a pointer to another (possibly the same) vertex. Starting at
an arbitrary stone on the board, following these pointers should
traverse the entire string in an arbitrary order before coming back to
the starting point. As for the 'string_number' array, this information
is invalid at empty points on the board. This data structure has the
good properties of requiring fixed space (regardless of the number of
strings) and making it easy to add a new stone or join two strings.

   <p>Additionally the code makes use of some work variables:

<pre class="example">     static int ml[BOARDMAX];
     static int liberty_mark;
     static int string_mark;
     static int next_string;
     static int strings_initialized = 0;
</pre>
   <p>The <code>ml</code> array and <code>liberty_mark</code> are used to "mark" liberties on
the board, e.g. to avoid counting the same liberty twice. The convention is
that if <code>ml[pos]</code> has the same value as <code>liberty_mark</code>, then
<code>pos</code> is marked. To clear all marks it suffices to increase the value
of <code>liberty_mark</code>, since it is never allowed to decrease.

   <p>The same relation holds between the <code>mark</code> field of the <code>string_data</code>
structure and <code>string_mark</code>. Of course these are used for marking
individual strings.

   <p><code>next_string</code> gives the number of the next available entry in the
<code>string</code> array. Then <code>strings_initialized</code> is set to one when
all data structures are known to be up to date. Given an arbitrary board
position in the &lsquo;<samp><span class="samp">board</span></samp>&rsquo; array, this is done by calling
<code>incremental_board_init()</code>. It is not necessary to call this
function explicitly since any other function that needs the information
does this if it has not been done.

   <p>The interesting part of the code is the incremental update of the data
structures when a stone is played and subsequently removed. To
understand the strategies involved in adding a stone it is necessary
to first know how undoing a move works. The idea is that as soon as
some piece of information is about to be changed, the old value is
pushed onto a stack which stores the value and its address. The stack
is built from the following structures:

<pre class="example">     struct change_stack_entry {
       int *address;
       int value;
     };
     
     struct change_stack_entry change_stack[STACK_SIZE];
     int change_stack_index;
</pre>
   <p class="noindent">and manipulated with the macros

<pre class="example">     BEGIN_CHANGE_RECORD()
     PUSH_VALUE(v)
     POP_MOVE()
</pre>
   <p>Calling <code>BEGIN_CHANGE_RECORD()</code> stores a null pointer in the address
field to indicate the start of changes for a new move. As mentioned
earlier <code>PUSH_VALUE()</code> stores a value and its corresponding address. 
Assuming that all changed information has been duly pushed onto the
stack, undoing the move is only a matter of calling <code>POP_MOVE()</code>,
which simply assigns the values to the addresses in the reverse order
until the null pointer is reached. This description is slightly
simplified because this stack can only store 'int' values and we need
to also store changes to the board. Thus we have two parallel stacks
where one stores <code>int</code> values and the other one stores
<code>Intersection</code> values.

   <p>When a new stone is played on the board, first captured opponent
strings, if any, are removed. In this step we have to push the board
values and the <code>next_stone</code> pointers for the removed stones, and
update the liberties and neighbor lists for the neighbors of the
removed strings. We do not have to push all information in the
'string' entries of the removed strings however. As we do not reuse
the entries they will remain intact until the move is pushed and they
are back in use.

   <p>After this we put down the new stone and get three distinct cases:

     <ol type=1 start=1>
<li>The new stone is isolated, i.e. it has no friendly neighbor. 
<li>The new stone has exactly one friendly neighbor. 
<li>The new stone has at least two friendly neighbors.
        </ol>

   <p>The first case is easiest. Then we create a new string by using the
number given by <code>next_string</code> and increasing this variable. The string
will have size one, <code>next_stone</code> points directly back on itself, the
liberties can be found by looking for empty points in the four
directions, possible neighbor strings are found in the same way, and
those need also to remove one liberty and add one neighbor.

   <p>In the second case we do not create a new string but extend the
neighbor with the new stone. This involves linking the new stone into
the cyclic chain, if needed moving the origin, and updating liberties
and neighbors. Liberty and neighbor information also needs updating
for the neighbors of the new stone.

   <p>In the third case finally, we need to join already existing strings. 
In order not to have to store excessive amounts of information, we
create a new string for the new stone and let it assimilate the
neighbor strings. Thus all information about those can simply be left
around in the 'string' array, exactly as for removed strings. Here it
becomes a little more complex to keep track of liberties and neighbors
since those may have been shared by more than one of the joined
strings. Making good use of marks it all becomes rather
straightforward anyway.

   <p>The often used construction

<pre class="example">         pos = FIRST_STONE(s);
         do {
             ...
             pos = NEXT_STONE(pos);
         } while (!BACK_TO_FIRST_STONE(s, pos));
</pre>
   <p class="noindent">traverses the stones of the string with number &lsquo;<samp><span class="samp">s</span></samp>&rsquo; exactly once,
with <code>pos</code> holding the coordinates. In general <code>pos</code> is
used as board coordinate and &lsquo;<samp><span class="samp">s</span></samp>&rsquo; as an index into the
<code>string</code> array or sometimes a pointer to an entry in the
<code>string</code> array.

   </body></html>

