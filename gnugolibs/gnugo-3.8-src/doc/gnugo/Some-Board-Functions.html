<html lang="en">
<head>
<title>Some Board Functions - GNU Go Documentation</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name="description" content="GNU Go Documentation">
<meta name="generator" content="makeinfo 4.13">
<link title="Top" rel="start" href="index.html#Top">
<link rel="up" href="Libboard.html#Libboard" title="Libboard">
<link rel="prev" href="Incremental-Board.html#Incremental-Board" title="Incremental Board">
<link href="http://www.gnu.org/software/texinfo/" rel="generator-home" title="Texinfo Homepage">
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css"><!--
  pre.display { font-family:inherit }
  pre.format  { font-family:inherit }
  pre.smalldisplay { font-family:inherit; font-size:smaller }
  pre.smallformat  { font-family:inherit; font-size:smaller }
  pre.smallexample { font-size:smaller }
  pre.smalllisp    { font-size:smaller }
  span.sc    { font-variant:small-caps }
  span.roman { font-family:serif; font-weight:normal; } 
  span.sansserif { font-family:sans-serif; font-weight:normal; } 
--></style>
</head>
<body>
<div class="node">
<a name="Some-Board-Functions"></a>
<p>
Previous:&nbsp;<a rel="previous" accesskey="p" href="Incremental-Board.html#Incremental-Board">Incremental Board</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="Libboard.html#Libboard">Libboard</a>
<hr>
</div>

<h3 class="section">15.4 Some Board Functions</h3>

<p><strong>Reading</strong>, often called <strong>search</strong> in computer game
theory, is a fundamental process in GNU Go. This is the process
of generating hypothetical future boards in order to determine
the answer to some question, for example "can these stones live." 
Since these are hypothetical future positions, it is important
to be able to undo them, ultimately returning to the present
board. Thus a move stack is maintained during reading. When
a move is tried, by the function <code>trymove</code>, or its
variant <code>tryko</code>. This function pushes the current board
on the stack and plays a move. The stack pointer <code>stackp</code>,
which keeps track of the position, is incremented. The function
<code>popgo()</code> pops the move stack, decrementing <code>stackp</code> and
undoing the last move made.

   <p>Every successful <code>trymove()</code> must be matched with a <code>popgo()</code>. 
Thus the correct way of using this function is:

<pre class="example">     
       if (trymove(pos, color, ... )) {
            ...    [potentially lots of code here]
            popgo();
       }
</pre>
   <p class="noindent">In case the move is a ko capture, the legality of the capture is subject to
the komaster scheme (see <a href="Ko.html#Ko">Ko</a>).

     <ul>
<li><code>int trymove(int pos, int color, const char *message)</code>
<a name="index-trymove-289"></a><blockquote>
Returns true if <code>(pos)</code> is a legal move for <code>color</code>. In that
case, it pushes the board on the stack and makes the move, incrementing
<code>stackp</code>. If the reading code is recording reading variations (as
with <samp><span class="option">--decide-string</span></samp> or with <samp><span class="option">-o</span></samp>), the string
<code>*message</code> will be inserted in the SGF file as a comment. The
comment will also refer to the string at <code>str</code> if this is not
<code>0</code>. The value of <code>str</code> can be NO_MOVE if it is not needed but
otherwise the location of <code>str</code> is included in the comment. 
</blockquote>
     <li><code>int tryko(int pos, int color, const char *message)</code>
<a name="index-tryko-290"></a><blockquote>
<code>tryko()</code> pushes the position onto the stack, and makes a move
<code>pos</code> of <code>color</code>. The move is allowed even if it is an
illegal ko capture. It is to be imagined that <code>color</code> has made an
intervening ko threat which was answered and now the continuation is to
be explored. Return 1 if the move is legal with the above
caveat. Returns zero if it is not legal because of suicide. 
</blockquote>

     <li><code>void popgo()</code>
<a name="index-popgo-291"></a><blockquote>
Pops the move stack. This function must (eventually) be called after a
succesful <code>trymove</code> or <code>tryko</code> to restore the board
position. It undoes all the changes done by the call to
<code>trymove/tryko</code> and leaves the board in the same state as it was
before the call.

     <p><strong>NOTE</strong>: If <code>trymove/tryko</code> returns <code>0</code>, i.e. the tried
move was not legal, you must <strong>not</strong> call <code>popgo</code>. 
</blockquote>

     <li><code>int komaster_trymove(int pos, int color, const char *message, int str, int *is_conditional_ko, int consider_conditional_ko)</code>
<a name="index-komaster_005ftrymove-292"></a><blockquote>
Variation of <code>trymove</code>/<code>tryko</code> where ko captures (both
conditional and unconditional) must follow a komaster scheme
(see <a href="Ko.html#Ko">Ko</a>). 
</blockquote>

   </ul>

   <p>As you see, <code>trymove()</code> plays a move which can be easily
retracted (with <code>popgo()</code>) and it is call thousands of
times per actual game move as GNU Go analyzes the board position. 
By contrast the function <code>play_move()</code> plays a move which
is intended to be permanent, though it is still possible to
undo it if, for example, the opponent retracts a move.

     <ul>
<li><code>void play_move(int pos, int color)</code>
<a name="index-play_005fmove-293"></a><blockquote>
Play a move. If you want to test for legality you should first call
<code>is_legal()</code>. This function strictly follows the algorithm:
          <ol type=1 start=1>
<li>Place a stone of given color on the board. 
<li>If there are any adjacent opponent strings without liberties,
remove them and increase the prisoner count. 
<li>If the newly placed stone is part of a string without liberties,
remove it and increase the prisoner count.
          </ol>
In spite of the name &ldquo;permanent move&rdquo;, this move can (usually) be
unplayed by <code>undo_move()</code>, but it is significantly more costly than
unplaying a temporary move. There are limitations on the available
move history, so under certain circumstances the move may not be
possible to unplay at a later time. 
</blockquote>
     <li><code>int undo_move(int n)</code>
<a name="index-undo_005fmove-294"></a><blockquote>
Undo &lsquo;<samp><span class="samp">n</span></samp>&rsquo; permanent moves. Returns 1 if successful and 0 if it fails. 
If &lsquo;<samp><span class="samp">n</span></samp>&rsquo; moves cannot be undone, no move is undone. 
</blockquote>
     </ul>

   <p>Other board functions are documented in See <a href="Board-Utilities.html#Board-Utilities">Board Utilities</a>.

   </body></html>

