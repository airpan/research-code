<html lang="en">
<head>
<title>Tuning - GNU Go Documentation</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name="description" content="GNU Go Documentation">
<meta name="generator" content="makeinfo 4.13">
<link title="Top" rel="start" href="index.html#Top">
<link rel="up" href="Patterns.html#Patterns" title="Patterns">
<link rel="prev" href="Connection-Functions.html#Connection-Functions" title="Connection Functions">
<link rel="next" href="PM-Implementation.html#PM-Implementation" title="PM Implementation">
<link href="http://www.gnu.org/software/texinfo/" rel="generator-home" title="Texinfo Homepage">
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css"><!--
  pre.display { font-family:inherit }
  pre.format  { font-family:inherit }
  pre.smalldisplay { font-family:inherit; font-size:smaller }
  pre.smallformat  { font-family:inherit; font-size:smaller }
  pre.smallexample { font-size:smaller }
  pre.smalllisp    { font-size:smaller }
  span.sc    { font-variant:small-caps }
  span.roman { font-family:serif; font-weight:normal; } 
  span.sansserif { font-family:sans-serif; font-weight:normal; } 
--></style>
</head>
<body>
<div class="node">
<a name="Tuning"></a>
<p>
Next:&nbsp;<a rel="next" accesskey="n" href="PM-Implementation.html#PM-Implementation">PM Implementation</a>,
Previous:&nbsp;<a rel="previous" accesskey="p" href="Connection-Functions.html#Connection-Functions">Connection Functions</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="Patterns.html#Patterns">Patterns</a>
<hr>
</div>

<h3 class="section">9.11 Tuning the Pattern databases</h3>

<p><a name="index-tuning-the-pattern-database-180"></a><a name="index-tuning-the-shapes-database-181"></a>
Since the pattern databases, together with the valuation of move
reasons, decide GNU Go's personality, much time can be devoted to
&ldquo;tuning&rdquo; them. Here are some suggestions.

   <p>If you want to experiment with modifying the pattern database, invoke
with the <samp><span class="option">-a</span></samp> option. This will cause every pattern to be evaluated,
even when some of them may be skipped due to various optimizations.

   <p>You can obtain a Smart Game Format (SGF) record of your game in at least
two different ways. One is to use CGoban to record the game. You can
also have GNU Go record the game in Smart Game Format, using the <samp><span class="option">-o</span></samp>
option. It is best to combine this with <samp><span class="option">-a</span></samp>. Do not try to read the SGF
file until the game is finished and you have closed the game
window. This does not mean that you have to play the game out to its
conclusion. You may close the CGoban window on the game and GNU Go
will close the SGF file so that you can read it.

   <p>If you record a game in SGF form using the <samp><span class="option">-o</span></samp> option, GNU Go will add
labels to the board to show all the moves it considered, with their
values. This is an extremely useful feature, since one can see at a
glance whether the right moves with appropriate weights are being
proposed by the move generation.

   <p>First, due to a bug of unknown nature, it occasionally happens
that GNU Go will not receive the <code>SIGTERM</code> signal from CGoban that it
needs to know that the game is over. When this happens, the SGF file
ends without a closing parenthesis, and CGoban will not open the
file. You can fix the file by typing:

<pre class="example">     
      echo ")" &gt;&gt;[filename]
     
</pre>
   <p class="noindent">at the command line to add this closing parenthesis. Or you could
add the ) using an editor.

   <p>Move values exceeding 99 (these should be rare) can be displayed by
CGoban but you may have to resize the window in order to see all three
digits. Grab the lower right margin of the CGoban window and pull it
until the window is large. All three digits should be visible.

   <p>If you are playing a game without the <samp><span class="option">-o</span></samp> option and you wish to
analyze a move, you may still use CGoban's &ldquo;Save Game&rdquo; button to get
an SGF file. It will not have the values of the moves labelled, of
course.

   <p>Once you have a game saved in SGF format, you can analyze any
particular move by running:

<pre class="example">     
       gnugo -l [filename] -L [move number] -t -a -w
     
</pre>
   <p class="noindent">to see why GNU Go made that move, and if you make changes to the
pattern database and recompile the program, you may ask GNU Go to
repeat the move to see how the behavior changes. If you're using
emacs, it's a good idea to run GNU Go in a shell in a buffer (M-x
shell) since this gives good navigation and search facilities.

   <p>Instead of a move number, you can also give a board coordinate to <samp><span class="option">-L</span></samp>
in order to stop at the first move played at this location. If you
omit the <samp><span class="option">-L</span></samp> option, the move after those in the file will be
considered.

   <p>If a bad move is proposed, this can have several reasons. To begin
with, each move should be valued in terms of actual points on the
board, as accurately as can be expected by the program. If it's not,
something is wrong. This may have two reasons. One possibility is that
there are reasons missing for the move or that bogus reasons have been
found. The other possibility is that the move reasons have been
misevaluated by the move valuation functions. Tuning of patterns is
with a few exceptions a question of fixing the first kind of problems.

   <p>If there are bogus move reasons found, search through the trace output
for the pattern that is responsible. (Some move reasons, e.g. most
tactical attack and defense, do not originate from patterns. If no
pattern produced the bogus move reason, it is not a tuning problem.) 
Probably this pattern was too general or had a faulty constraint. Try
to make it more specific or correct bugs if there were any. If the
pattern and the constraint looks right, verify that the tactical
reading evaluates the constraint correctly. If not, this is either a
reading bug or a case where the reading is too complicated for GNU Go.

   <p>If a connecting move reason is found, but the strings are already
effectively connected, there may be missing patterns in <samp><span class="file">conn.db</span></samp>. 
Similarly, worms may be incorrectly amalgamated due to some too
general or faulty pattern in <samp><span class="file">conn.db</span></samp>. To get trace output from the
matching of patterns in <samp><span class="file">conn.db</span></samp> you need to add a second
<samp><span class="option">-t</span></samp> option.

   <p>If a move reason is missing, there may be a hole in the database. It
could also be caused by some existing pattern being needlessly
specific, having a faulty constraint, or being rejected due to a
reading mistake. Unless you are familiar with the pattern databases,
it may be hard to verify that there really is a pattern missing. Look
around the databases to try to get a feeling for how they are
organized. (This is admittedly a weak point of the pattern databases,
but the goal is to make them more organized with time.) If you decide
that a new pattern is needed, try to make it as general as possible,
without allowing incorrect matches, by using proper classification
from among snOoXx and constraints. The reading functions can be put to
good use. The reason for making the patterns as general as they can be
is that we need a smaller number of them then, which makes the
database much easier to maintain. Of course, if you need too
complicated constraints, it's usually better to split the pattern.

   <p>If a move has the correct set of reasons but still is misevaluated,
this is usually not a tuning problem. There are, however, some
possibilities to work around these mistakes with the use of patterns. 
In particular, if the territorial value is off because <code>delta_terri()</code>
give strange results, the (min)terri and maxterri values can be set by
patterns as a workaround. This is typically done by the endgame
patterns, where we can know the (minimum) value fairly well from the
pattern. If it should be needed, (min)value and maxvalue can be used
similarly. These possibilities should be used conservatively though,
since such patterns are likely to become obsolete when better (or at
least different) functions for e.g. territory estimation are being
developed.

   <p>In order to choose between moves with the same move reasons, e.g. 
moves that connect two dragons in different ways, patterns with a
nonzero shape value should be used. These should give positive shape
values for moves that give good shape or good aji and negative values
for bad shape and bad aji. Notice that these values are additive, so
it's important that the matches are unique.

   <p>Sente moves are indicated by the use of the pattern followup value. 
This can usually not be estimated very accurately, but a good rule is
to be rather conservative. As usual it should be measured in terms of
actual points on the board. These values are also additive so the same
care must be taken to avoid unintended multiple matches.

   <p>You can also get a visual display of the dragons using the <samp><span class="option">-T</span></samp>
option. The default GNU Go configuration tries to build a
version with color support using either curses or the
ansi escape sequences. You are more likely to find color
support in rxvt than xterm, at least on many systems, so
we recommend running:

<pre class="example">       gnugo -l [filename] -L [move number] -T
</pre>
   <p class="noindent">in an rxvt window. If you do not see a color display,
and if your host is a GNU/Linux machine, try this again
in the Linux console.

   <p>Worms belonging to the same dragon are labelled with the same letters. 
The colors indicate the value of the field <code>dragon.safety</code>, which
is set in <samp><span class="file">moyo.c</span></samp>.

<pre class="format">Green:  GNU Go thinks the dragon is alive
Yellow: Status unknown
Blue:   GNU Go thinks the dragon is dead
Red:    Status critical (1.5 eyes) or weak by the algorithm
        in <samp><span class="file">moyo.c</span></samp>
</pre>
   <p><a name="index-eliminate-the-randomness-182"></a>
If you want to get the same game over and over again, you can
eliminate the randomness in GNU Go's play by providing a fixed
random seed with the <samp><span class="option">-r</span></samp> option.

   </body></html>

