<html lang="en">
<head>
<title>Monte Carlo Go - GNU Go Documentation</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name="description" content="GNU Go Documentation">
<meta name="generator" content="makeinfo 4.13">
<link title="Top" rel="start" href="index.html#Top">
<link rel="prev" href="Influence.html#Influence" title="Influence">
<link rel="next" href="Libboard.html#Libboard" title="Libboard">
<link href="http://www.gnu.org/software/texinfo/" rel="generator-home" title="Texinfo Homepage">
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css"><!--
  pre.display { font-family:inherit }
  pre.format  { font-family:inherit }
  pre.smalldisplay { font-family:inherit; font-size:smaller }
  pre.smallformat  { font-family:inherit; font-size:smaller }
  pre.smallexample { font-size:smaller }
  pre.smalllisp    { font-size:smaller }
  span.sc    { font-variant:small-caps }
  span.roman { font-family:serif; font-weight:normal; } 
  span.sansserif { font-family:sans-serif; font-weight:normal; } 
--></style>
</head>
<body>
<div class="node">
<a name="Monte-Carlo-Go"></a>
<p>
Next:&nbsp;<a rel="next" accesskey="n" href="Libboard.html#Libboard">Libboard</a>,
Previous:&nbsp;<a rel="previous" accesskey="p" href="Influence.html#Influence">Influence</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="index.html#Top">Top</a>
<hr>
</div>

<h2 class="chapter">14 Monte Carlo Go</h2>

<p><a name="index-Monte-Carlo-Go-263"></a><a name="index-UCT-algorithm-264"></a>In Monte Carlo Go the engine plays random games to the
end, generating moves from a pattern database within
the context of the algorithm UCT (upper confidence
bounds applied to trees).  This algorithm allowed the
program MoGo (<a href="http://www.lri.fr/~gelly/MoGo.htm">http://www.lri.fr/~gelly/MoGo.htm</a>,
to become the first computer program to defeat a
professional while taking a 9 stone handicap
(<a href="http://senseis.xmp.net/?MoGo">http://senseis.xmp.net/?MoGo</a>).

   <p>GNU Go 3.8 can play 9x9 Go with the option
<samp><span class="option">--monte-carlo</span></samp> using the UCT algorithm. 
For command line options, see See <a href="Invoking-GNU-Go.html#Invoking-GNU-Go">Invoking GNU Go</a>.

   <p>During reading, the engine makes incremental updates
of local 3x3 neighborhood, suicide status, self-atari
status, and number of stones captured, for each move.

   <p>GNU Go's simulations (Monte Carlo games) are pattern generated. 
The random playout move generation is distributed
strictly proportional to move values computed by table
lookup from a local context consisting of 3x3
neighborhood, opponent suicide status, own and
opponent self-atari status, number of stones captured
by own and opponent move, and closeness to the
previous move. Let's call this local context simply "a
pattern" and the table "pattern values" or simply
"patterns".

   <p>There are three built-in databases that you can select
using the option <samp><span class="option">--mc-patterns &lt;name&gt;</span></samp>, where
<samp><span class="option">&lt;name&gt;</span></samp> is one of

     <ul>
<li><samp><span class="command">mc_montegnu_classic</span></samp>
<li><samp><span class="command">mc_mogo_classic</span></samp>
<li><samp><span class="command">mc_uniform</span></samp>
</ul>

   <p>The first of these is an approximation of the previous random move
generation algorithm. The <samp><span class="command">mogo_classic</span></samp> pattern values is an
approximation of the simulation policy used by early versions of MoGo,
as published in the report <a href="http://hal.inria.fr/inria-00117266">odification of UCT with Patterns in Monte-Carlo Go</a>
RR-6062, by Sylvain Gelly, Yizao Wang, Rémi Munos, and
Olivier Teytaud. The uniform pattern values is the so
called "light" playout which chooses uniformly between
all legal moves except single point proper eyes.

   <p>If you're not satisfied with these you can also tune your own
pattern values with a pattern database file and load it at runtime
with <samp><span class="option">--mc-load-patterns &lt;name&gt;</span></samp> adding your own
pattern database.

   <p>Let's start with the uniform pattern values. Those are defined by the
file <samp><span class="file">patterns/mc_uniform.db</span></samp>, which looks like this:

<pre class="example">     
     oOo
     O*O
     oO?
     
     :0
     
     oOo
     O*O
     ---
     
     :0
     
     |Oo
     |*O
     +--
     
     :0
</pre>
   <p>Patterns are always exactly 3x3 in size with the move at the center
point. The symbols are the usual for GNU Go pattern databases:

<pre class="example">     * move
     O own stone (i.e. the same color as the color to move)
     o own stone or empty
     X opponent stone
     x opponent stone or empty
     ? own stone, opponent stone, or empty
     | vertical edge
     - horizontal edge
     + corner
</pre>
   <p>There's also a new symbol:

<pre class="example">     % own stone, opponent stone, empty, or edge
</pre>
   <p>After the pattern comes a line starting with a colon. In all these
patterns it says that the pattern has a move value of 0, i.e. must not
be played. Unmatched patterns have a default value of 1. When all move
values are zero for both players, the playout will stop. Including the
three patterns above is important because otherwise the playouts would
be likely to go on indefinitely, or as it actually happens be
terminated at a hard-coded limit of 600 moves. Also place these
patterns at the top of the database because when multiple patterns
match, the first one is used, regardless of the values.

   <p>When using only these patterns you will probably notice that it plays
rather heavy, trying hard to be solidly connected. This is because
uniform playouts are badly biased with a high probability of non-solid
connections being cut apart. To counter this you could try a pattern
like

<pre class="example">     ?X?
     O*O
     x.?
     
     :20,near
</pre>
   <p>to increase the probability that the one-point jump is reinforced when
threatened. Here we added the property "near", which means that the
pattern only applies if the previous move was played "near" this move. 
Primarily "near" means within the surrounding 3x3 neighborhood but it
also includes certain cases of liberties of low-liberty strings
adjacent to the previous move, e.g. the move to extend out of an atari
created by the previous move. You have to read the source to find out
the exact rules for nearness.

   <p>We could also be even more specific and say

<pre class="example">     ?X?
     O*O
     x.?
     
     :20,near,osafe,xsafe
</pre>
   <p>to exclude the cases where this move is a self atari (osafe) or would
be a self-atari for the opponent (xsafe).

   <p>It may also be interesting to see the effect of capturing stones. A
catch-all pattern for captures would be

<pre class="example">     ?X%
     ?*%
     %%%
     
     :10,ocap1,osafe
     :20,ocap2
     :30,ocap3
</pre>
   <p>where we have used multiple colon lines to specify different move
values depending on the number of captured stones; value 10 for a
single captured stone, value 20 for two captured stones, and value 30
for three or more captured stones. Here we also excluded self-atari
moves in the case of 1 captured stone in order to avoid getting stuck
in triple-ko in the playouts (there's no superko detection in the
playouts).

   <p>The full set of pattern properties is as follows:

     <dl>
<dt><code>near</code><a name="index-near-265"></a><dd>The move is "near" the previous move.

     <br><dt><code>far</code><a name="index-far-266"></a><dd>The move is not "near" the previous move.

     <br><dt><code>osafe</code><a name="index-osafe-267"></a><dd>The move is not a self-atari.

     <br><dt><code>ounsafe</code><a name="index-ounsafe-268"></a><dd>The move is a self-atari.

     <br><dt><code>xsafe</code><a name="index-xsafe-269"></a><dd>The move would not be a self-atari for the opponent.

     <br><dt><code>xunsafe</code><a name="index-xunsafe-270"></a><dd>The move would be a self-atari for the opponent.

     <br><dt><code>xsuicide</code><a name="index-xsuicide-271"></a><dd>The move would be suicide for the opponent

     <br><dt><code>xnosuicide</code><a name="index-xnosuicide-272"></a><dd>The move would not be suicide for the opponent.

     <br><dt><code>ocap0</code><a name="index-ocap0-273"></a><dd>The move captures zero stones.

     <br><dt><code>ocap1</code><a name="index-ocap1-274"></a><dd>The move captures one stone.

     <br><dt><code>ocap2</code><a name="index-ocap2-275"></a><dd>The move captures two stones.

     <br><dt><code>ocap3</code><a name="index-ocap3-276"></a><dd>The move captures three or more stones.

     <br><dt><code>ocap1+</code><a name="index-ocap1_002b-277"></a><dd>The move captures one or more stones.

     <br><dt><code>ocap1-</code><a name="index-ocap1_002d-278"></a><dd>The move captures at most one stone.

     <br><dt><code>ocap2+</code><a name="index-ocap2_002b-279"></a><dd>The move captures two or more stones.

     <br><dt><code>ocap2-</code><a name="index-ocap2_002d-280"></a><dd>The move captures at most two stones.

     <br><dt><code>xcap0</code><a name="index-xcap0-281"></a><dd>An opponent move would capture zero stones.

     <br><dt><code>xcap1</code><a name="index-xcap1-282"></a><dd>An opponent move would capture one stone.

     <br><dt><code>xcap2</code><a name="index-xcap2-283"></a><dd>An opponent move would capture two stones.

     <br><dt><code>xcap3</code><a name="index-xcap3-284"></a><dd>An opponent move would capture three or more stones.

     <br><dt><code>xcap1+</code><a name="index-xcap1_002b-285"></a><dd>An opponent move would capture one or more stones.

     <br><dt><code>xcap1-</code><a name="index-xcap1_002d-286"></a><dd>An opponent move would capture at most one stone.

     <br><dt><code>xcap2+</code><a name="index-xcap2_002b-287"></a><dd>An opponent move would capture two or more stones.

     <br><dt><code>xcap2-</code><a name="index-xcap2_002d-288"></a><dd>An opponent move would capture at most two stones. 
</dl>

   <p>These can be combined arbitrarily but all must be satisfied for the
pattern to take effect. If contradictory properties are combined, the
pattern will never match.

<h4 class="subsection">14.0.1 Final Remarks</h4>

     <ul>
<li>Move values are unsigned 32-bit integers. To avoid overflow in
computations it is highly recommended to keep the values below
10000000 or so. 
<li>There is no speed penalty for having lots of patterns in the
database. The average time per move is approximately constant
(slightly dependent on how often stones are captured or become low
on liberties) and the time per game mostly depends on the average
game length. 
<li>For more complex pattern databases, see
<samp><span class="file">patterns/mc_montegnu_classic.db</span></samp> and <samp><span class="file">patterns/mc_mogo_classic.db</span></samp>. 
</ul>

   <p>Nobody really knows how to tune the random playouts to get as strong
engine as possible. Please play with this and report any interesting
findings, especially if you're able to make it substantially stronger
than the <samp><span class="file">montegnu_classic</span></samp> patterns.

   </body></html>

