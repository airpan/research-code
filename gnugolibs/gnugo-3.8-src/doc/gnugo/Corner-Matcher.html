<html lang="en">
<head>
<title>Corner Matcher - GNU Go Documentation</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name="description" content="GNU Go Documentation">
<meta name="generator" content="makeinfo 4.13">
<link title="Top" rel="start" href="index.html#Top">
<link rel="up" href="Patterns.html#Patterns" title="Patterns">
<link rel="prev" href="Ladders-in-Joseki.html#Ladders-in-Joseki" title="Ladders in Joseki">
<link rel="next" href="Editing-Patterns.html#Editing-Patterns" title="Editing Patterns">
<link href="http://www.gnu.org/software/texinfo/" rel="generator-home" title="Texinfo Homepage">
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css"><!--
  pre.display { font-family:inherit }
  pre.format  { font-family:inherit }
  pre.smalldisplay { font-family:inherit; font-size:smaller }
  pre.smallformat  { font-family:inherit; font-size:smaller }
  pre.smallexample { font-size:smaller }
  pre.smalllisp    { font-size:smaller }
  span.sc    { font-variant:small-caps }
  span.roman { font-family:serif; font-weight:normal; } 
  span.sansserif { font-family:sans-serif; font-weight:normal; } 
--></style>
</head>
<body>
<div class="node">
<a name="Corner-Matcher"></a>
<p>
Next:&nbsp;<a rel="next" accesskey="n" href="Editing-Patterns.html#Editing-Patterns">Editing Patterns</a>,
Previous:&nbsp;<a rel="previous" accesskey="p" href="Ladders-in-Joseki.html#Ladders-in-Joseki">Ladders in Joseki</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="Patterns.html#Patterns">Patterns</a>
<hr>
</div>

<h3 class="section">9.18 Corner Matcher</h3>

<p><a name="index-implementation-of-pattern-matching-192"></a><a name="index-joseki-193"></a><a name="index-corner-matcher-194"></a>
GNU Go uses a special matcher for joseki patterns.  It has certain constraints
on the patterns it can match, but is much faster and takes far less space to
store patterns than the standard matcher.

   <p>Patterns used with corner matcher have to qualify the following conditions:

     <ul>
<li>They must be matchable only at a corner of the board (hence the name of
the matcher). 
<li>They can consist only of &lsquo;<samp><span class="samp">O</span></samp>&rsquo;, &lsquo;<samp><span class="samp">X</span></samp>&rsquo; and &lsquo;<samp><span class="samp">.</span></samp>&rsquo; elements. 
<li>Of all pattern values (see <a href="Pattern-Values.html#Pattern-Values">Pattern Values</a>), corner matcher only
support <code>shape(x)</code>.  This is not because the matcher cannot handle other
values in principle, just they are currently not used in joseki databases. 
</ul>

   <p>Corner matcher was specifically designed for joseki patterns and they of
course satisfy all the conditions above.  With some modifications corner
matcher could be used for fuseki patterns as well, but fullboard matcher
does its work just fine.

   <p>The main idea of the matcher is very same to the one of DFA matcher
(see <a href="Pattern-matching-with-DFA.html#Pattern-matching-with-DFA">Pattern matching with DFA</a>): check all available patterns at once,
not a single pattern at a time.  A modified version of DFA matcher could be
used for joseki pattern matching, but its database would be very large. 
Corner matcher capitalizes on the fact that there are relatively few
stones in each such pattern.

   <p>Corner pattern database is organized into a tree.  Nodes of the tree are
called &ldquo;variations&rdquo;.  Variations represent certain sets of stones in a
corner of the board.  Root variation corresponds to an empty corner and a
step down the tree is equivalent to adding a stone to the corner.  Each
variation has several properties:

     <ul>
<li>stone position relative to the corner,
<li>a flag determining whether the stone color must be equal to the first
matched stone color,
<li>number of stones in the corner area (see below) of the variation stone. 
</ul>

   <p>By corner area we define a rectangle which corners are the current corner of
the board and the position of the stone (inclusive).  For instance, if the
current board corner is A19 then corner area of a stone at C18 consists
of A18, A19, B18, B19, C18 and C19.

   <p>Variation which is a direct child of the root variation matches if there
is any stone at the variation position and the stone is alone in its
corner area.

   <p>Variation at a deeper level of the tree matches if there is a stone of
specified color in variation position and the number of stones in its
corner area is equal to the number specified in variation structure.

   <p>When a certain variation matches, all its children has to be checked
recursively for a match.

   <p>All leaf variations and some inner ones have patterns attached to them. 
For a pattern to match, it is required that its <em>parent</em> variation
matches.  In addition, it is checked that pattern is being matched for
the appropriate color (using its variation &ldquo;stone color&rdquo; field) and
that the number of stones in the area where the pattern is being matched
is indeed equal to the number of stones in the pattern.  The &ldquo;stone
position&rdquo; property of the pattern variation determines the move suggested
by the pattern.

   <p>Consider this joseki pattern which has four stones:

<pre class="example">     ------+
     ......|
     ......|
     .O*...|
     .XXO..|
     ......|
     ......|
</pre>
   <p>To encode it for the corner matcher, we have to use five variations,
each next being a child of previous:

   <p><table summary=""><tr align="left"><td valign="top">Tree level </td><td valign="top">Position </td><td valign="top">Color </td><td valign="top">Number of stones
<br></td></tr><tr align="left"><td valign="top">1 </td><td valign="top">R16 </td><td valign="top">&ldquo;same&rdquo; </td><td valign="top">1
<br></td></tr><tr align="left"><td valign="top">2 </td><td valign="top">P17 </td><td valign="top">&ldquo;same&rdquo; </td><td valign="top">1
<br></td></tr><tr align="left"><td valign="top">3 </td><td valign="top">Q16 </td><td valign="top">&ldquo;other&rdquo; </td><td valign="top">2
<br></td></tr><tr align="left"><td valign="top">4 </td><td valign="top">P16 </td><td valign="top">&ldquo;other&rdquo; </td><td valign="top">4
<br></td></tr><tr align="left"><td valign="top">5 </td><td valign="top">Q17 </td><td valign="top">&ldquo;same&rdquo; </td><td valign="top">1
   <br></td></tr></table>

   <p>The fifth variation should have an attached pattern.  Note that the stone
color for the fifth variation is &ldquo;same&rdquo; because the first matched stone
for this pattern is &lsquo;<samp><span class="samp">O</span></samp>&rsquo; which stands for the stones of the player
to whom moves are being suggested with &lsquo;<samp><span class="samp">*</span></samp>&rsquo;.

   <p>The tree consists of all variations for all patterns combined together. 
Variations for each patterns are sorted to allow very quick tree branch
rejection and at the same time keep the database small enough.  More
details can be found in comments in file <samp><span class="file">mkpat.c</span></samp>

   <p>Corner matcher resides in <samp><span class="file">matchpat.c</span></samp> in two functions:
<code>corner_matchpat()</code> and <code>do_corner_matchpat()</code>.  The former computes
<code>num_stones[]</code> array which holds number of stones in corner areas of
different intersections of the board for all possible transformations. 
<code>corner_matchpat()</code> also matches top-level variations. 
<code>do_corner_matchpat()</code> is responsible for recursive matching on the
variation tree and calling callback function upon pattern match.

   <p>Tree-like database for corner matcher is generated by <code>mkpat</code> program. 
Database generator consists of several functions, most important are:
<code>corner_best_element()</code>, <code>corner_variation_new()</code>,
<code>corner_follow_variation()</code> and <code>corner_add_pattern()</code>.

   </body></html>

