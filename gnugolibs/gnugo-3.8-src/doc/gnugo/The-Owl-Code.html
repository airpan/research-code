<html lang="en">
<head>
<title>The Owl Code - GNU Go Documentation</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name="description" content="GNU Go Documentation">
<meta name="generator" content="makeinfo 4.13">
<link title="Top" rel="start" href="index.html#Top">
<link rel="up" href="Pattern-Based-Reading.html#Pattern-Based-Reading" title="Pattern Based Reading">
<link rel="next" href="Combinations.html#Combinations" title="Combinations">
<link href="http://www.gnu.org/software/texinfo/" rel="generator-home" title="Texinfo Homepage">
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css"><!--
  pre.display { font-family:inherit }
  pre.format  { font-family:inherit }
  pre.smalldisplay { font-family:inherit; font-size:smaller }
  pre.smallformat  { font-family:inherit; font-size:smaller }
  pre.smallexample { font-size:smaller }
  pre.smalllisp    { font-size:smaller }
  span.sc    { font-variant:small-caps }
  span.roman { font-family:serif; font-weight:normal; } 
  span.sansserif { font-family:sans-serif; font-weight:normal; } 
--></style>
</head>
<body>
<div class="node">
<a name="The-Owl-Code"></a>
<p>
Next:&nbsp;<a rel="next" accesskey="n" href="Combinations.html#Combinations">Combinations</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="Pattern-Based-Reading.html#Pattern-Based-Reading">Pattern Based Reading</a>
<hr>
</div>

<h3 class="section">12.1 The Owl Code</h3>

<p>The life and death code in <samp><span class="file">optics.c</span></samp>, described elsewhere
(see <a href="Eyes.html#Eyes">Eyes</a>), works reasonably well as long as the position is in a
<dfn>terminal position</dfn>, which we define to be one where there are no
moves left which can expand the eye space, or limit it. In situations
where the dragon is surrounded, yet has room to thrash around a bit
making eyes, a simple application of the graph-based analysis will not
work. Instead, a bit of reading is needed to reach a terminal position.

   <p>The defender tries to expand his eyespace, the attacker to limit
it, and when neither finds an effective move, the position is
evaluated. We call this type of life and death reading
<dfn>Optics With Limit-negotiation</dfn> (OWL). The module which
implements it is in <samp><span class="file">engine/owl.c</span></samp>.

   <p>There are two reasonably small databases
<samp><span class="file">patterns/owl_defendpats.db</span></samp> and <samp><span class="file">patterns/owl_attackpats.db</span></samp>
of expanding and limiting moves. The code in <samp><span class="file">owl.c</span></samp> generates a
small move tree, allowing the attacker only moves from
<samp><span class="file">owl_attackpats.db</span></samp>, and the defender only moves from
<samp><span class="file">owl_defendpats.db</span></samp>. In addition to the moves suggested by
patterns, vital moves from the eye space analysis are also tested.

   <p>A third database, <samp><span class="file">owl_vital_apats.db</span></samp> includes patterns which
override the eyespace analysis done by the optics code. Since the
eyeshape graphs ignore the complications of shortage of liberties and
cutting points in the surrounding chains, the static analysis of
eyespace is sometimes wrong. The problem is when the optics code says
that a dragon definitely has 2 eyes, but it isn't true due to
shortage of liberties, so the ordinary owl patterns never get into play. 
In such situations <samp><span class="file">owl_vital_apats.db</span></samp> is the only available measure
to correct mistakes by the optics. Currently the patterns in
<samp><span class="file">owl_vital_apats.db</span></samp> are only matched when the level is 9 or
greater.

   <p>The owl code is tuned by editing these three pattern databases,
principally the first two.

   <p><a name="index-owl_005fattack-251"></a><a name="index-owl_005fdefend-252"></a><a name="index-compute_005feyes_005fpessimistic-253"></a>A node of the move tree is considered <code>terminal</code> if no further moves
are found from <samp><span class="file">owl_attackpats.db</span></samp> or <samp><span class="file">owl_defendpats.db</span></samp>, or if
the function <code>compute_eyes_pessimistic()</code> reports that the group is
definitely alive. At this point, the status of the group is evaluated. 
The functions <code>owl_attack()</code> and <code>owl_defend()</code>, with
usage similar to <code>attack()</code> and <code>find_defense()</code>, make
use of the owl pattern databases to generate the move tree and decide
the status of the group.

   <p>The function <code>compute_eyes_pessimistic()</code> used by the owl
code is very conservative and only feels certain about eyes if the
eyespace is completely closed (i.e. no marginal vertices).

   <p>The maximum number of moves tried at each node is limited by
the parameter <code>MAX_MOVES</code> defined at the beginning of
<samp><span class="file">engine/owl.c</span></samp>. The most most valuable moves are
tried first, with the following restrictions:

     <ul>
<li>If <code>stackp &gt; owl_branch_depth</code> then only one move is tried per
variation. 
<li>If <code>stackp &gt; owl_reading_depth</code> then the reading terminates,
and the situation is declared a win for the defender (since
deep reading may be a sign of escape). 
<li>If the node count exceeds <code>owl_node_limit</code>, the reading also
terminates with a win for the defender. 
<li>Any pattern with value 99 is considered a forced move: no
other move is tried, and if two such moves are found, the function
returns false. This is only relevant for the attacker. 
<li>Any pattern in <samp><span class="file">patterns/owl_attackpats.db</span></samp> and
<samp><span class="file">patterns/owl_defendpats.db</span></samp> with value 100 is considered a win: if
such a pattern is found by <code>owl_attack</code> or <code>owl_defend</code>, the
function returns true. This feature must be used most carefully. 
</ul>

   <p>The functions <code>owl_attack()</code> and <code>owl_defend()</code> may, like
<code>attack()</code> and <code>find_defense()</code>, return an attacking or
defending move through their pointer arguments. If the position is
already won, <code>owl_attack()</code> may or may not return an attacking
move. If it finds no move of interest, it will return <code>PASS</code>, that
is, <code>0</code>. The same goes for <code>owl_defend()</code>.

   <p>When <code>owl_attack()</code> or <code>owl_defend()</code> is called,
the dragon under attack is marked in the array <code>goal</code>. 
The stones of the dragon originally on the board are marked
with goal=1; those added by <code>owl_defend()</code> are marked
with goal=2. If all the original strings of the original dragon
are captured, <code>owl_attack()</code> considers the dragon to be defeated,
even if some stones added later can make a live group.

   <p>Only dragons with small escape route are studied when the
functions are called from <code>make_dragons()</code>.

   <p>The owl code can be conveniently tested using the
<samp><span class="option">--decide-owl </span><var>location</var></samp> option. This should be used with
<samp><span class="option">-t</span></samp> to produce a useful trace, <samp><span class="option">-o</span></samp> to produce
an SGF file of variations produced when the life and death of
the dragon at <var>location</var> is checked, or both. 
<samp><span class="option">--decide-position</span></samp> performs the same analysis for all
dragons with small escape route.

   </body></html>

