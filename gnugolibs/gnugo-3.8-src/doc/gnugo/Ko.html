<html lang="en">
<head>
<title>Ko - GNU Go Documentation</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name="description" content="GNU Go Documentation">
<meta name="generator" content="makeinfo 4.13">
<link title="Top" rel="start" href="index.html#Top">
<link rel="up" href="Tactical-Reading.html#Tactical-Reading" title="Tactical Reading">
<link rel="prev" href="Persistent-Cache.html#Persistent-Cache" title="Persistent Cache">
<link rel="next" href="A-Ko-Example.html#A-Ko-Example" title="A Ko Example">
<link href="http://www.gnu.org/software/texinfo/" rel="generator-home" title="Texinfo Homepage">
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css"><!--
  pre.display { font-family:inherit }
  pre.format  { font-family:inherit }
  pre.smalldisplay { font-family:inherit; font-size:smaller }
  pre.smallformat  { font-family:inherit; font-size:smaller }
  pre.smallexample { font-size:smaller }
  pre.smalllisp    { font-size:smaller }
  span.sc    { font-variant:small-caps }
  span.roman { font-family:serif; font-weight:normal; } 
  span.sansserif { font-family:sans-serif; font-weight:normal; } 
--></style>
</head>
<body>
<div class="node">
<a name="Ko"></a>
<p>
Next:&nbsp;<a rel="next" accesskey="n" href="A-Ko-Example.html#A-Ko-Example">A Ko Example</a>,
Previous:&nbsp;<a rel="previous" accesskey="p" href="Persistent-Cache.html#Persistent-Cache">Persistent Cache</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="Tactical-Reading.html#Tactical-Reading">Tactical Reading</a>
<hr>
</div>

<h3 class="section">11.4 Ko Handling</h3>

<p>The principles of ko handling are the same for tactical reading and
owl reading.

   <p>We have already mentioned (see <a href="Reading-Basics.html#Reading-Basics">Reading Basics</a>) that GNU Go
uses a return code of <code>KO_A</code> or <code>KO_B</code> if the result depends on
ko. The return code of <code>KO_B</code> means that the position can be won
provided the player whose move calls the function can come up
with a sufficiently large ko threat. In order to verify this,
the function must simulate making a ko threat and having it
answered by taking the ko even if it is illegal. We call such an
experimental taking of the ko a <dfn>conditional</dfn> ko capture.

   <p>Conditional ko captures are accomplished by the function <code>tryko()</code>. 
This function is like <code>trymove()</code> except that
it does not require legality of the move in question.

   <p>The static reading functions, and the global functions <code>do_attack</code>
and <code>do_find_defense</code> consult parameters <code>komaster</code>,
<code>kom_pos</code>, which are declared static in <samp><span class="file">board.c</span></samp>. These mediate ko
captures to prevent the occurrence of infinite loops. During
reading, the komaster values are pushed and popped from a stack.

   <p>Normally <code>komaster</code> is <code>EMPTY</code> but it can also be
&lsquo;<samp><span class="samp">BLACK</span></samp>&rsquo;, &lsquo;<samp><span class="samp">WHITE</span></samp>&rsquo;, <code>GRAY_BLACK</code>, <code>GRAY_WHITE</code> or
<code>WEAK_KO</code>. The komaster is set to <code>color</code> when <code>color</code> makes a
conditional ko capture. In this case <code>kom_pos</code> is set to the location of
the captured ko stone.

   <p>If the opponent is komaster, the reading functions will not try to
take the ko at <code>kom_pos</code>. Also, the komaster is normally not
allowed to take another ko. The exception is a nested ko, characterized
by the condition that the captured ko stone is at distance 1 both
vertically and horizontally from <code>kom_pos</code>, which is the location
of the last stone taken by the komaster. Thus in this situation:

<pre class="example">     
              .OX
              OX*X
             OmOX
              OO
     
</pre>
   <p>Here if &lsquo;<samp><span class="samp">m</span></samp>&rsquo; is the location of <code>kom_pos</code>, then the move at
&lsquo;<samp><span class="samp">*</span></samp>&rsquo; is allowed.

   <p>The rationale behind this rule is that in the case where there are
two kos on the board, the komaster cannot win both, and by becoming
komaster he has already chosen which ko he wants to win. But in the
case of a nested ko, taking one ko is a precondition to taking the
other one, so we allow this.

   <p>If the komaster's opponent takes a ko, then both players have taken one ko. In
this case <code>komaster</code> is set to <code>GRAY_BLACK</code> or <code>GRAY_WHITE</code> and
after this further ko captures are even further restricted.

   <p>If the ko at <code>kom_pos</code> is filled, then the komaster reverts to
<code>EMPTY</code>.

   <p>In detail, the komaster scheme is as follows. Color &lsquo;<samp><span class="samp">O</span></samp>&rsquo; is to move. 
This scheme is known as scheme 5 since in versions of GNU Go through
3.4, several different schemes were included.

     <ul>
<li>1. Komaster is EMPTY.
          <ul>
<li>1a. Unconditional ko capture is allowed. 
<blockquote>
Komaster remains EMPTY if previous move was not a ko capture. 
Komaster is set to WEAK_KO if previous move was a ko capture
and kom_pos is set to the old value of board_ko_pos. 
</blockquote>
          <li>1b) Conditional ko capture is allowed. 
<blockquote>
Komaster is set to O and kom_pos to the location of the ko, where a stone was
just removed. 
</blockquote>
          </ul>
     <li>2. Komaster is O:
          <ul>
<li>2a) Only nested ko captures are allowed. Kom_pos is moved to the
new removed stone. 
<li>2b) If komaster fills the ko at kom_pos then komaster reverts to
EMPTY. 
</ul>
     <li>3. Komaster is X:
<blockquote>
Play at kom_pos is not allowed. Any other ko capture
is allowed. If O takes another ko, komaster becomes GRAY_X. 
</blockquote>
     <li>4. Komaster is GRAY_O or GRAY_X:
<blockquote>
Ko captures are not allowed. If the ko at kom_pos is
filled then the komaster reverts to EMPTY. 
</blockquote>
     <li>5. Komaster is WEAK_KO:
          <ul>
<li>5a) After a non-ko move komaster reverts to EMPTY. 
<li>5b) Unconditional ko capture is only allowed if it is nested ko capture. 
<blockquote>
Komaster is changed to WEAK_X and kom_pos to the old value of
board_ko_pos. 
</blockquote>
          <li>5c) Conditional ko capture is allowed according to the rules of 1b. 
</ul>
     </ul>

   </body></html>

