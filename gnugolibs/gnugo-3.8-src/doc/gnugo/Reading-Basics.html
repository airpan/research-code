<html lang="en">
<head>
<title>Reading Basics - GNU Go Documentation</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name="description" content="GNU Go Documentation">
<meta name="generator" content="makeinfo 4.13">
<link title="Top" rel="start" href="index.html#Top">
<link rel="up" href="Tactical-Reading.html#Tactical-Reading" title="Tactical Reading">
<link rel="next" href="Hashing.html#Hashing" title="Hashing">
<link href="http://www.gnu.org/software/texinfo/" rel="generator-home" title="Texinfo Homepage">
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css"><!--
  pre.display { font-family:inherit }
  pre.format  { font-family:inherit }
  pre.smalldisplay { font-family:inherit; font-size:smaller }
  pre.smallformat  { font-family:inherit; font-size:smaller }
  pre.smallexample { font-size:smaller }
  pre.smalllisp    { font-size:smaller }
  span.sc    { font-variant:small-caps }
  span.roman { font-family:serif; font-weight:normal; } 
  span.sansserif { font-family:sans-serif; font-weight:normal; } 
--></style>
</head>
<body>
<div class="node">
<a name="Reading-Basics"></a>
<p>
Next:&nbsp;<a rel="next" accesskey="n" href="Hashing.html#Hashing">Hashing</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="Tactical-Reading.html#Tactical-Reading">Tactical Reading</a>
<hr>
</div>

<h3 class="section">11.1 Reading Basics</h3>

<p>What we call <em>Tactical Reading</em> is the analysis whether there is
a direct capture of a single string, or whether there is a move to prevent
such a direct capture.

   <p>If the reading module finds out that the string can get captured, this
answer should (usually) be trusted. However, if it says it can be defended,
this does not say as much. It is often the case that such a string has
no chance to make a life, but that it cannot be captured within the
horizon (and the cutoff heuristics) of the tactical reading.

   <p>The tactical reading is done by the functions in <samp><span class="file">engine/reading.c</span></samp>. 
It is a minimax search that declares win for the attacker once he can
physically take the string off board, whereas the defense is considered
successful when the string has sufficiently many liberties. A string with
five liberties is always considered alive. At higher depth within the
search tree even fewer liberties cause GNU Go to give up the attack,
See <a href="depthparams.html#depthparams">depthparams</a>.

   <p>The reading code makes use of a stack onto which board positions can
be pushed. The parameter <code>stackp</code> is zero if GNU Go is
examining the true board position; if it is higher than zero, then
GNU Go is examining a hypothetical position obtained by playing
several moves.

   <p>The most important public reading functions are <code>attack</code> and
<code>find_defense</code>. These are wrappers for functions <code>do_attack</code> and
<code>do_find_defense</code> which are declared statically in <samp><span class="file">reading.c</span></samp>. The
functions <code>do_attack</code> and <code>do_find_defense</code> call each other
recursively.

<h4 class="subsection">11.1.1 Organization of the reading code</h4>

<p>The function <code>do_attack</code> and <code>do_find_defense</code> are wrappers
themselves and call <code>attack1</code>, <code>attack2</code>, <code>attack3</code> or
<code>attack4</code> resp.  <code>defend1</code>, <code>defend1</code>, <code>defend1</code>
or <code>defend1</code> depending on the number of liberties.

   <p>These are fine-tuned to generate and try out the moves in an efficient
order. They generate a few moves themselves (mostly direct liberties
of the string), and then call helper functions called <code>..._moves</code>
which suggest less obvious moves. Which of these functions get called
depends on the number of liberties and of the current search depth.

<h4 class="subsection">11.1.2 Return Codes</h4>

<p><a name="Return-Codes"></a><a name="index-return-codes-215"></a><a name="index-reading-return-codes-216"></a>
The return codes of the reading (and owl) functions and owl can
be <code>0</code>, <code>KO_B</code>, <code>KO_A</code> or <code>WIN</code>. Each reading
function determines whether a particular player (assumed to have the
move) can solve a specific problem, typically attacking or defending
a string.

   <p>A return code of <code>WIN</code> means success, 0 failure, while <code>KO_A</code> and
<code>KO_B</code> are success conditioned on ko. A function returns <code>KO_A</code>
if the position results in ko and that the player to move
will get the first ko capture (so the opponent has to make the
first ko threat). A return code of <code>KO_B</code> means that the player
to move will have to make the first ko threat.

   <p><a name="Experimental-Owl-Extension"></a>If GNU Go is compiled with the configure option
<samp><span class="option">--enable-experimental-owl-ext</span></samp> then the owl functions also have
possible return codes of <code>GAIN</code> and <code>LOSS</code>. A code of <code>GAIN</code>
means that the attack (or defense) does not succeed, but that in the process
of trying to attack or defend, an opponent's worm is captured. A code
of <code>LOSS</code> means that the attack or defense succeeds, but that another
friendly worm dies during the attack or defense.

<h4 class="subsection">11.1.3 Reading cutoff and depth parameters</h4>

<p><a name="depthparams"></a>Depth of reading is controlled by the parameters <code>depth</code>
and <code>branch_depth</code>. The <code>depth</code> has a default value
<code>DEPTH</code> (in <samp><span class="file">liberty.h</span></samp>), which is set to 16 in the
distribution, but it may also be set at the command line using
the <samp><span class="option">-D</span></samp> or <samp><span class="option">--depth</span></samp> option.  If <code>depth</code> is
increased, GNU Go will be stronger and slower. GNU Go will read
moves past depth, but in doing so it makes simplifying
assumptions that can cause it to miss moves.

   <p>Specifically, when <code>stackp &gt; depth</code>, GNU Go assumes that as
soon as the string can get 3 liberties it is alive. This
assumption is sufficient for reading ladders.

   <p>The <code>branch_depth</code> is typically set a little below <code>depth</code>. 
Between <code>branch_depth</code> and <code>depth</code>, attacks on strings with
3 liberties are considered, but branching is inhibited, so fewer
variations are considered.

   <p>%<a name="index-small_005fsemeai-217"></a>%Currently the reading code does not try to defend a string by
%attacking a boundary string with more than two liberties. Because
%of this restriction, it can make oversights. A symptom of this is
%two adjacent strings, each having three or four liberties, each
%classified as <code>DEAD</code>. To resolve such situations, a function
%<code>small_semeai()</code> (in <samp><span class="file">engine/semeai.c</span></samp>) looks for such
%pairs of strings and corrects their classification.

   <p>The <code>backfill_depth</code> is a similar variable with a default 12. Below
this depth, GNU Go will try "backfilling" to capture stones. 
For example in this situation:

<pre class="example">     
     .OOOOOO.    on the edge of the board, O can capture X but
     OOXXXXXO    in order to do so he has to first play at a in
     .aObX.XO    preparation for making the atari at b. This is
     --------    called backfilling.
</pre>
   <p>Backfilling is only tried with <code>stackp &lt;= backfill_depth</code>. The
parameter <code>backfill_depth</code> may be set using the <samp><span class="option">-B</span></samp>
option.

   <p>The <code>fourlib_depth</code> is a parameter with a default of only 7. 
Below this depth, GNU Go will try to attack strings with
four liberties. The <code>fourlib_depth</code> may be set using the
<samp><span class="option">-F</span></samp> option.

   <p>The parameter <code>ko_depth</code> is a similar cutoff. If
<code>stackp&lt;ko_depth</code>, the reading code will make experiments
involving taking a ko even if it is not legal to do so (i.e., it
is hypothesized that a remote ko threat is made and answered
before continuation).  This parameter may be set using the
<samp><span class="option">-K</span></samp> option.

   <p><a name="index-reading_002ec-218"></a>
     <ul>
<li><code>int attack(int str, int *move)</code>
<a name="index-attack-219"></a><blockquote>
Determines if the string at <code>str</code> can
be attacked, and if so, <code>*move</code> returns the attacking move,
unless <code>*movei</code> is a null pointer. (Use null pointers if
you are interested in the result of the attack but not the
attacking move itself.) Returns <code>WIN</code>, if the attack succeeds,
0 if it fails, and <code>KO_A</code> or <code>KO_B</code> if the result depends on ko
<a href="Return-Codes.html#Return-Codes">Return Codes</a>. 
</blockquote>
     <a name="index-find_005fdefense-220"></a><li><code>find_defense(int str, int *move)</code>
<blockquote>
Attempts to find a move that will save the string at <code>str</code>. It
returns true if such a move is found, with <code>*move</code> the location
of the saving move (unless <code>*move</code> is a null pointer). It is not
checked that tenuki defends, so this may give an erroneous answer if
<code>!attack(str)</code>.  Returns <code>KO_A</code> or <code>KO_B</code> if the
result depends on ko See <a href="Return-Codes.html#Return-Codes">Return Codes</a>. 
</blockquote>
     <a name="index-safe_005fmove-221"></a><li><code>safe_move(int str, int color)</code> :
<blockquote>
The function <code>safe_move(str, color)</code> checks whether a move at
<code>str</code> is illegal or can immediately be captured. If <code>stackp==0</code>
the result is cached. If the move only can be captured by a ko, it's
considered safe. This may or may not be a good convention. 
</blockquote>
     </ul>

   </body></html>

