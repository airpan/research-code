cdef extern from "engine/board.h":
    ctypedef unsigned char Intersection

    cdef struct board_state:
        int board_size
        Intersection board[BOARDSIZE]
        int board_ko_pos
        int black_captured
        int white_captured

        Intersection initial_board[BOARDSIZE]
        int initial_board_ko_pos
        int initial_white_captured
        int initial_black_captured
        int move_history_color[MAX_MOVE_HISTORY]
        int move_history_pos[MAX_MOVE_HISTORY]
        Hash_data move_history_hash[MAX_MOVE_HISTORY]
        int move_history_pointer

        float komi
        int handicap
        int move_number

    void clear_board(void);
    int test_gray_border(void);
    void setup_board(Intersection new_board[MAX_BOARD][MAX_BOARD], int ko_pos,
                     int *last, float new_komi, int w_captured, int b_captured);
    void add_stone(int pos, int color);
    void remove_stone(int pos);
    void play_move(int pos, int color);
    int undo_move(int n);

    int trymove(int pos, int color, const char *message, int str)

    int is_pass(int pos)
    int is_legal(int pos, int color)
    int is_suicide(int pos, int color)
    int is_illegal_ko_capture(int pos, int color)
    int is_allowed_move(int pos, int color)
    int is_ko(int pos, int color, int *ko_pos)
    int is_ko_point(int pos)
    int does_capture_something(int pos, int color)
    int is_self_atari(int pos, int color)


