import platform
USING_PYPY = platform.python_implementation() == 'PyPy'
USING_THEANO = not USING_PYPY

if USING_THEANO:
    import theano
    import theano.tensor as T
import numpy as np
import random
from cfour import ConnectFourState
import cPickle
import math

class LinearValueFunction:
    # linear value function that allows for td learning
    def __init__(self, weights, bias, alpha=0.01, epsilon=0.20, discount=1, comp_hyper=0.1):
        """
        weights = numpy.array, with dtype=theano.config.floatX
        bias = float
        """
        self.alpha = alpha
        self.comp_hyper = comp_hyper # L2 norm
        self.epsilon = epsilon
        self.bias = bias

        self.weights = weights
        if USING_THEANO:
            self.feat = T.vector()
            self._w = T.vector()
            self._b = T.scalar()
            self.lin_output = T.dot(self.feat, self._w) + self._b

    def getValue(self, state):
        features = state.toFeatures()
        return sum(f*w for f,w in zip(features, self.weights)) + self.bias

    def getValues(self, state):
        # returns a list of (action, value) pairs with player sign accounted for
        # leaves state unchanged at end
        # ALWAYS CALL THIS METHOD (makes it easier to make sure state is not changed)
        values = []
        sgn = 1 if state.getPlayer() == 1 else -1
        for a in state.getActions():
            values.append( (a, sgn * self.getValue(state.doAction(a))) )
            state.reverseAction()
        return values

    def getGreedyAction(self, state):
        values = self.getValues(state)
        bestVal = max(values, key=lambda v:v[1])[1]
        bestActions = [a for a,v in values if v == bestVal]
        return random.choice(bestActions)

    def getEpsilonGreedyAction(self, state):
        if random.random() < self.epsilon:
            return state.getRandomAction()
        return self.getGreedyAction(state)

    def getEpsilonProportionalAction(self, state, beta=10):
        # with epsilon chance, choose an action proportional to its value (softmax)
        # otherwise choose best action
        values = self.getValues(state)
        if random.random() < self.epsilon:
            values = [(a, math.exp(beta * v)) for a,v in values]
            total = sum(v[1] for v in values)
            ran = random.random() * total
            for a, v in values:
                if ran < v:
                    return a
                ran -= v
        # do greedy
        bestVal = max(values, key=lambda v: v[1])[1]
        bestActions = [a for a,v in values if v == bestVal]
        return random.choice(bestActions)

    def valUpdate(self, features, expected):
        val = sum(f * w for f, w in zip(features, self.weights)) + self.bias
        diff = val - expected
        # V = w_1f_1 +w_2f_2 + ... + w_nf_n
        # loss function = sum_i^n (b+w_1f_1 + ... + w_nf_n - y_i)^2
        # gradient for weight i = 2 * (stuff) * f_i
        # for bias, 2 * stuff
        self.bias -= 2 * self.alpha * diff
        self.weights = [w - self.alpha * (self.comp_hyper * w + 2 * diff * f) for f, w in zip(features, self.weights)]
        # use L2 norm
        # partial of sum_i^n w_i^2 = 2w_i

    def getParams(self):
        p = list(self.weights)
        p.append(self.bias)
        return p

def learn_one_game(cfour_value_func):
    state = ConnectFourState()
    while not state.isTerminal():
        action = cfour_value_func.getAction(state)
        nextState = state.doAction(action)
        cfour_value_func.update(
            state, action, nextState
        )
        state = nextState
    return state.getWinner()

def trainOnSamples(vf):
    random.shuffle(train_data)
    for feat, value in train_data:
        vf.valUpdate(feat, value)

def error(vf):
    err = 0
    for feat, value in test_data:
        err += (vf.bias + sum(f*w for f,w in zip(feat, vf.weights)) - value) ** 2
    return float(err) / len(test_data)

if __name__ == '__main__':
    with open("iter_test_data") as f:
        test_data = cPickle.load(f)

    with open("iter_train_data") as f:
        train_data = cPickle.load(f)

    print "Samples loaded"
    import argparse
    parser = argparse.ArgumentParser(description='MCTS testing')
    parser.add_argument('--alpha', dest='alpha', action='store', type=float)
    parser.add_argument('--hyper', dest='hyper', action='store', type=float)
    args = parser.parse_args()
    print 'Testing with alpha=%f, hyper=%f' % (args.alpha, args.hyper)

    if USING_THEANO:
        start_weights = np.zeros(42, dtype=theano.config.floatX)
    else:
        start_weights = [0] * 42

    value_func = LinearValueFunction(
        weights=start_weights,
        bias=0,
        alpha=args.alpha,
        comp_hyper=args.hyper,
    )

# stop if no improvement after 100 run throughs
# (check every 100)
    best_error = 10000000
    best_weights = None
    iters = 0
    while True:
        start = best_error
        for _ in xrange(200):
            trainOnSamples(value_func)
            e = error(value_func) 
            if math.isnan(e):
                raise ValueError('Went out of control')
            print "MSE = %f" % e
        if e < best_error:
            best_weights = list(value_func.getParams())
            best_error = e
        iters += 200
        print "%d runs, best MSE = %f" % (iters, best_error)
        print best_weights
        if best_error == start:
            print "Stoppping"
            f = open("weights_iter_trained", "w")
            f.write(str(best_weights) + "\n")
            f.close()
            break

"""
import time
start_time = time.time()


# run for an hour
# TODO dont' hardcode time and general style fix

num_games = 0
wins = [0,0,0]
while time.time() - start_time < 3600:
    wins[learn_one_game(value_func)] += 1
    num_games += 1
    if num_games % 100 == 0:
        print "%d games played, time %f minutes" % (num_games, (time.time() - start_time) / 60.0)

print "Draws %d P1 wins %d P2 wins %d" % (wins[0], wins[1], wins[2])
print value_func.getParams()
f = open("weights", "w")
f.write(str(value_func.getParams()) + "\n")
f.close()

"""
