from math import *
from game import GameState
from util import Counter
import platform
# check if running in PyPy
USING_PYPY = platform.python_implementation() == 'PyPy'

if USING_PYPY:
    pass
else:
    import numpy as np
    import theano
import random
rows = 12
cols = 12

class GomokuState(GameState):
    def __init__(self, player=1):
        """
        Player = 1 or 2
        board = 0 for empty, 1 for player 1 piece, 2 for player 2 piece
        actions = a column number from 0 to 6
        reward = +1 for player 1 win, -1 for player 2 win, 0 for a draw

        0 1 2 3 4 5
        6 7 8 9 10 11
        and so on...
        """
        GameState.__init__(self, player)
        self.board = [ [0] * cols for _ in xrange(rows) ]
        self.cols = cols
        self.rows = rows
        self.history = [0] * (rows * cols)
        self.nacts = 0

    def doAction(self, action, y=None):
        if y is None:
            x, y = action // self.cols, action % self.cols
        else:
            x, y = action, y
        # actions are a single index of format x-coor + (y * cols)
        # modifies the state in place and returns self
        self.board[x][y] = self.player
        self.player = 3 - self.player
        self.reward = None
        self.history[self.nacts] = x * self.cols + y
        self.nacts += 1

        return self

    def reverseAction(self):
        # reverse the previous action
        action = self.history[self.nacts - 1]
        self.nacts -= 1
        self.board[action // self.cols][action % self.cols] = 0
        self.player = 3 - self.player
        return self

    def getActions(self):
        if self.isTerminal():
            return []
        return [i * self.cols + j for i in xrange(self.rows) for j in xrange(self.cols) if self.board[i][j] == 0]

    def isLegal(self, action):
        return self.board[action // self.cols][action % self.cols] == 0

    def isTerminal(self):
        # only need to check last piece added
        if self.nacts < 5:
            return False
        i = self.history[self.nacts - 1]
        i, j = i // self.cols, i % self.cols

        # Horizontals
        # Note that first index picks the row
        for x in xrange(max(0, j-4), min(self.cols - 4, j+1)):
            if self.board[i][x] == self.board[i][x+1] == self.board[i][x+2] == self.board[i][x+3] == self.board[i][x+4]:
        #        print 'ho'
                return True

        # Verticals
        for y in xrange(max(0, i-4), min(self.rows - 4, i+1)):
            if self.board[y][j] == self.board[y+1][j] == self.board[y+2][j] == self.board[y+3][j] == self.board[y+4][j]:
        #        print 've'
                return True

        # Diag top left to bottom right
        # x and y are bottom most piece of diag
        y = i - 4
        x = j - 4
        while x < 0 or y < 0:
            x += 1
            y += 1
        while x + 4 < self.cols and y +4 < self.rows and x <= j and y <= i:
            if self.board[y][x] == self.board[y+1][x+1] == self.board[y+2][x+2] == self.board[y+3][x+3] == self.board[y+4][x+4]:
        #        print 'di1'
                return True
            y += 1
            x += 1

        # Diag top right to bottom left
        y = i - 4
        x = j + 4
        while x >= self.cols or y < 0:
            x -= 1
            y += 1
        while x - 4 >= 0 and y + 4 < self.rows and x >= j and y <= i:
            if self.board[y][x] == self.board[y+1][x-1] == self.board[y+2][x-2] == self.board[y+3][x-3] == self.board[y+4][x-4]:
        #        print 'di2'
                return True
            y += 1
            x -= 1

        # Draw case
        if self.nacts == rows * cols:
            self.reward = 0
            return True
        return False

    def getReward(self):
        # Zero sum game, 1 = win for maxmizer, -1 = win for minimizer
        # next player is losing player if this is terminal
        if self.reward is not None:
            return self.reward
        return 1 if self.player == 2 else -1

    def getRewardForAction(self, action):
        n = self.doAction(action)
        if not n.isTerminal():
            return 0
        return n.getReward()

    def getWinner(self):
        if self.getReward() == 0:
            return 0
        return 3 - self.player

    def toFeatures(self):
        # return two bitvectors
        # first is all white squares, second is all black ones
        cells = reduce(lambda x,y: x + y, self.board)
        def w(n):
            return 1 if n == 1 else 0
        def b(n):
            return 1 if n == 2 else 0
        if USING_PYPY:
            return [w(c) for c in cells] + [b(c) for c in cells]
        else:
            return np.array([w(c) for c in cells] + [b(c) for c in cells], dtype=theano.config.floatX)

    def toFlippedFeatures(self):
        # return the exact opposite convention as the above
        # use when we need to pretend player 2 is player 1
        cells = reduce(lambda x,y: x + y, self.board)
        def w(n):
            return 1 if n == 2 else 0
        def b(n):
            return 1 if n == 1 else 0
        if USING_PYPY:
            return [w(c) for c in cells] + [b(c) for c in cells]
        else:
            return np.array([w(c) for c in cells] + [b(c) for c in cells], dtype=theano.config.floatX)

    def __str__(self):
        header = ['  ']
        for i in xrange(self.rows):
            header.append('%3d' % i)
        header = ''.join(header)
        to_print = [header]
        for i in xrange(self.rows):
            row = (self.board[i][j] for j in xrange(self.cols))
            row_st = []
            row_st.append('%3d' % i)
            row_st.extend('|.|' if c == 0 else '|O|' if c == 1 else '|X|' for c in row)
            to_print.append(''.join(row_st))
        return '\n'.join(to_print)

if __name__ == '__main__':
    g = GomokuState()
    used = set()
    import random
    while not g.isTerminal():
        x, y = random.randint(0, g.rows-1), random.randint(0, g.cols-1)
        if (x,y) not in used:
            used.add( (x,y) )
            g.doAction( x,y)
    print g
