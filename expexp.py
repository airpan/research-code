from cfour import ConnectFourState # not explicitly used, but as a reminder
import random

def randomStopPoint(state, horizon):
    t = random.randint(1, horizon)
    nacts = 0
    while t > 0 and not state.isTerminal():
        state.doAction(state.getRandomAction())
        nacts += 1
        t -= 1
    if state.isTerminal():
        state.reverseActions(nacts)
        return randomStopPoint(state, horizon)
    return state

