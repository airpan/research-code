files = ['UCTVF', 'policyonly', 'depthUCT', 'hybrid']
import matplotlib.pyplot as plt

def avg_cost(f, name):
    total = 0
    num = 0
    indices = []
    error = []
    for line in f:
        if 'Average cost' in line:
            num += 1
            indices.append(num)
            error.append(float(line.split("Average cost:")[1].strip()))
    avg_error = sum(error) / float(len(error))
    print "Average error:: %f" % avg_error
    plt.title("Error for %s" % name)
    plt.xlabel("Number of training games")
    plt.ylabel("Avg KL divergence over entire game")
    plt.plot(indices, error)
    plt.axhline(avg_error, color='k', linewidth=2)
    plt.show()

for name in files:
    print 'Error for %s' % name
    with open(name + "_out") as f:
        avg_cost(f, name)
        print '---------------------'
