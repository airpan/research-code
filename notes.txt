defaultTreePolicy sometimes makes moves that are guaranteed loss
- (moves that lose next turn with correct play, but only tends to show up in board states where bot is losing badly)
- (or, the potential gain from suboptimal opponent play from random playouts is good enough to bias the simulated win %)
Usually blocks guaranteed wins
-- justification - with random simulation, given that the opponent does not block the win on their first reply, odds of losing game drop
-- (the potential winner gets the first chance to randomly pick the right move, then the potential blocker gets chance to pick the blocking move)
-- so, any non-blocking move on first reply seems to kill the win rate enough to make the block the best
-- is this necessarily always going to be true? probably depends on iteration count

UCTTreePolicy is not always blocking wins
-implementation error?
-from the same board state, it seems to occasionally make different moves
--explanation - after a starting set of playouts, UCT has biases towards exploitation
--criteria/parameter for exploitation is probably exploiting it too hard, leaving too few playouts on other actions to realize it should block a certain column
--(40000 playouts, most visited/likely move gets >39k, leaving only 1k for the rest of the columns)
--exploitation bias means that if a move briefly looks good after enough playouts, it gets selected most of the time
-otherwise, plays similarly, feels generally worse

Progressive Bias - high error during initial simulation
 - heuristic applied to node, H
 - if # visits = n, add H / (n+1) to reduce influence
 
Parameter Tuning
 - parameters are sometimes domain specific
 - if tuned offline, adds a multiplying factor to computation time
 - if tuned online, parameter biased by current game
 -- is that actually a problem? you may want more or less exploration at different points in the game
 -- tricky part is how to learn this parameter well, assuming you want that behavior
 
 
Value function V(S) = (v,b), v = value, b = belief in accuracy of that value
range of b? probably 0 to 1
0 = absolutely no trust
1 = fully trust (fully accurate)
How about values in between?

When value function has finished learning, and if v* = true value, let

b = e^(-|(v* - v)|)

(Making stuff up here)

Now, suppose we don't know v* because it's too computationally intensive - can we still learn b?
If we do know b, then what?
Change alpha based on b?
Incorporate b into policy?




Exploration vs exploitation.

In the current formulation, exploration and exploitation are kind of mixed within the MCTS
The dream of MCTS is that we choose an action, and get a perfectly representative
estimate of the win.
