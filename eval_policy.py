from cfour import ConnectFourState
#from pybrain.tools.xml.networkreader import NetworkReader
from randomize_policy import val
from learning_theano.generate_net import load_kl_net
import random
from math import exp
import math


def makeSoftmaxNeural(net):
    # same tuning as linear
    def val(features):
        return net.activate(features)[0]
    def policy(state):
        sgn = 1 if state.getPlayer() == 1 else -1
        values = [sgn * val(state.doAction(a).toFeatures()) for a in state.getActions()]
        nA = len(values)
        if nA == 1:
            return state.getActions()[0]
        diff = max(values) - min(values)
        # e^{beta * diff} / (e^{beta * diff} + nA - 1) = 0.8
        # solving gives
        beta = math.log(4 * (nA - 1)) / diff
        beta = min(beta, 100)

        # yay for consistent iteration order!
        values = [(a, exp(beta * v)) for a,v in zip(state.getActions(), values)]
        total = sum(v[1] for v in values)
        ran = random.random() * total
        for a, v in values:
            if ran < v:
                return a
            ran -= v
        raise ValueError("Neural should not get here")
    return policy

# Uses PyBrain's neural net syntax, do not use
"""
def makePolicyFromNet(net):
    def val(features):
        return net.activate(features)[0]
    def policy(state):
        vals = [(a, val(state.doAction(a).toFeatures())) for a in state.getActions()]
        if state.getPlayer() == 1:
            bestVal = max(vals, key=lambda v: v[1])[1]
        else:
            bestVal = min(vals, key=lambda v: v[1])[1]
        bestActions = [a for a, v in vals if v == bestVal]
        return random.choice(bestActions)
    return policy
"""

def seeGreedyGame(policy, start_state):
    while not start_state.isTerminal():
        print start_state
        start_state = start_state.doAction(policy(start_state))
    print start_state


def linPolicy(weights):
    def policy(state):
        values = [(a, val(state.doAction(a).toFeatures(), weights)) for a in state.getActions()]
        if state.getPlayer() == 1:
            bestVal = max(values, key=lambda v: v[1])[1]
        else:
            bestVal = min(values, key=lambda v: v[1])[1]
        bestActions = [a for a,v in values if v == bestVal]
        return random.choice(bestActions)
    return policy

def net_pol(vf):
    def policy(state):
        if state.player == 1:
            vals = vf( [state.toFeatures()] )[0]
        else:
            vals = vf( [state.toFlippedFeatures()] )[0]
        vals = list(enumerate(vals))
        vals.sort(key=lambda v: (-1) ** state.player * v[1])
        action = None
        print 'Move values:'
        for a, v in vals:
            if state.isLegal(a) and action is None:
                action = a
            print '\t %d: %f' % (a, v)
        return action
    return policy

if __name__ == '__main__':
    s = ConnectFourState()
    vf = load_kl_net("sto_net_params/playerflip/uctvf_policy_hybrid")
    seeGreedyGame(net_pol(vf), s)
