# This is a very simple implementation of the UCT Monte Carlo Tree Search algorithm in Python 2.7.
# The function UCT(rootstate, itermax, verbose = False) is towards the bottom of the code.
# It aims to have the clearest and simplest possible code, and for the sake of clarity, the code
# is orders of magnitude less efficient than it could be made, particularly by using a 
# state.GetRandomMove() or state.DoRandomRollout() function.
# 
# Written by Peter Cowling, Ed Powley, Daniel Whitehouse (University of York, UK) September 2012.
# 
# Licence is granted to freely use and distribute for any sensible/legal purpose so long as this comment
# remains in any distributed code.
# 
# For more information about Monte Carlo Tree Search check out our web site at www.mcts.ai

# The code found online was less general than this, so I made some changes to it

# !!!!
# TODO Get this working on GPU correctly
# When I switch this code to running on the GPU, it gets
# 3x times slower. My guess is that most of the time loss
# is in copying the featurized state/output back and forth between the GPU
# and the processor, since this is done on every
# evaluation of the net, and this happens
# thousands of times per move.
# To do this properly, I need to cache results
# or do some kind of lazy evaluation/batch evaluation,
# or directly manipulate the GPU output instead of
# converting it to a numpy array

import math
from math import *
from game import GameState
from cfour import ConnectFourState, TicTacToeState
from gnugolibs.go import GoState
import platform
from randomize_policy import makeEpsilonGreedyPolicyFromWeights, \
        makeSoftmaxPolicy, makeEpsilonGreedyPolicyFromVF, \
        makeSoftmaxPolicyFromVF
USING_PYPY = platform.python_implementation() == 'PyPy'
if not USING_PYPY:
    #from pybrain.tools.xml.networkreader import NetworkReader
#    from eval_policy import makePolicyFromNet, makeSoftmaxNeural
    from learning_theano.generate_net import create_net_value_function, \
            get_untrained_net, get_kl_net_to_train, load_kl_net
    import numpy as np
    import theano
import random
import argparse
import time
import copy
import cPickle
from linear_td import LinearValueFunction
from expexp import randomStopPoint

# set it smaller for now
NUM_PLAYOUTS = 5000

"""
High level code structure:
    the game tree is represented by a series of nodes
    These nodes store the last action taken, the parent node, a list
    of unexplored actions, and a map of {explored actions -> child node}

    These node objects never carry the game state!
    Because of how the game tree is traversed (either moving up or down a level,
    never random access), we can get away with only have one copy of the game state,
    and applying/unapplying actions in the traversal. We can then pass the game state
    to any functions that need to know what the next actions are
"""
class Node:
    """
    A node in the game tree
    """
    def __init__(self, state, lastAction = None, parent = None):
        self.untriedActions = state.getActions()
        self.player = state.player
        self.childNodes = []
        self.childMap = {}
        self.parent = parent
        self.lastAction = lastAction
        self.visits = 0
        self.qvalue = 0 # ??? is this right?
        self.depth = 1 if parent is None else 1 + parent.depth

    def addChildForAction(self, action, state):
        """
        Applies given action to game state in this node, then creates a
        child node and attaches to this one

        Returns the child node if created, and None if it isn't
        """
        if action in self.untriedActions:
            n = Node(state.doAction(action), lastAction=action, parent=self)
            self.childNodes.append(n)
            self.childMap[action] = n
            self.untriedActions.remove(action)
            return n
        raise ValueError('action %s is not in untried actions %s' % (str(action), str(self.untriedActions)))

    def goToChild(self, action):
        if action not in self.childMap:
            print action, self.childMap
        return self.childMap[action]

    def getPlayer(self):
        return self.player

    def _value(self, child):
        """ Returns reward of child from perspective of current node """
        return (-1) ** (self.getPlayer() - 1) * child.qvalue / float(child.visits)

    def leastWins(self, banned=set()):
        # For successive rejects
        # getting kinda spaghetti here
        valid = filter(lambda c: c.lastAction not in banned, self.childNodes)
        scores = [self._value(c) for c in valid]
        bestScore = min(scores)
        choices = filter(lambda c: self._value(c) == bestScore, valid)
        return random.choice(choices).lastAction

    def mostWins(self):
        # return child node with most wins
        # break ties randomly
        scores = [self._value(c) for c in self.childNodes]
        bestScore = max(scores)
        choices = filter(lambda c: self._value(c) == bestScore, self.childNodes)
        return random.choice(choices).lastAction

    def mostVisited(self):
        # return child node with most visits
        options = sorted(self.childNodes,
                key=lambda c: c.visits)
        return options[-1].lastAction

    def __repr__(self):
        return "[M:" + str(self.lastAction) + " W/V:" + str(self.qvalue) + "/" + str(self.visits) + " U:" + str(self.untriedActions) + "]"

    def TreeToString(self, indent):
        s = self.IndentString(indent) + str(self)
        for c in self.childNodes:
             s += c.TreeToString(indent+1)
        return s

    def IndentString(self,indent):
        s = "\n"
        for i in xrange (1,indent+1):
            s += "| "
        return s

    def ChildrenToString(self):
        s = ""
        # assume 2 player zero sum, print action chosen last
        for c in sorted(self.childNodes, key=lambda c: (-1)**(self.getPlayer()-1)*c.qvalue / float(c.visits)):
             s += str(c) + "\n"
        return s

# tree policy - given current node in search tree, choose child node
# (chooses how to get to fringe)
# the state argument is given to make sure the game state is properly
# modified

def defaultTreePolicy(node, state):
    # Randomly choose a child node
    next_node = random.choice(node.childMap.values())
    state.doAction(next_node.lastAction)
    return next_node

def uctScore(parent, child, param=10.0):
    # ONLY FOR TWO PLAYER ZERO SUM GAMES
    # the returned score must be from perspective of current player
    # (i.e. this accounts for minimizing player)
    # parent = current node, child = node to select
    if child.visits == 0:
        return float('inf')
    # convention has 2 = minimizer
    # invert all payout values
    return (-1) ** (parent.getPlayer()-1) * child.qvalue / float(child.visits) + param * sqrt(log(parent.visits) / child.visits)

def brokeScore(parent, child, param=10.0):
    if child.visits == 0:
        return float('inf')
    return child.qvalue / float(child.visits) + param * sqrt(log(parent.visits) / child.visits)

# Possible idea worth implementing -
# "When deeper inside the game tree, we want to play
# the stronger move more often, since we want it to
# contribute more to the backup for the root node"
# So, perhaps tweak the UCT exploration parameter
# to depend on depth?
def uctScoreWithDepth(parent, child, depth, param=10.0):
    # the deeper we are, the less we want to explore
    # this seems like an okay option (although it may decay very fast)
    return uctScore(parent, child, param=param/depth)

def key(node):
    return tuple(tuple(row) for row in node.state.board)

def standardUCT(node, state):
    s = sorted(node.childNodes, key = lambda c: uctScore(node, c))[-1]
    state.doAction(s.lastAction)
    return s

def depthUCT(node, state):
    s = sorted(node.childNodes, key = lambda c: uctScoreWithDepth(node, c, node.depth))[-1]
    state.doAction(s.lastAction)
    return s

def makeUCTTreePolicy(pol, UCT_param=10.0):
    """
    pol should take a state and output a probability distribution over moves
    Note that this is different from other parts of the code!
    """
    # the standard UCT node selection policy is entirely online
    # (score of node = online Q value + exploration term)
    # Given a policy that we are learning from MCTS,
    # we should be using some offline knowledge in choosing what
    # nodes to expand
    def treePol(node, state):
        # for now, a more exploitation based scheme
        # for later, consider a scheme that biases towards picking
        # actions with large differences between values
        if node.player == 1:
            policy_dist = pol( [state.toFeatures()] )[0]
        else:
            policy_dist = pol( [state.toFlippedFeatures()] )[0]
        # TODO there should be a hyperparam here to model trust in the original value function
        s = sorted(
            node.childNodes,
            key=lambda c: policy_dist[c.lastAction] + uctScoreWithDepth(node, c, node.depth, UCT_param),
        )
        best = s[-1]
        state.doAction(best.lastAction)
        return best
    return treePol


def chooseFromPolicy(pol, state):
    # choose an action based on the stochastic policy
    # policy takes a game state and returns a distribution
    # over what output move to take
    # the neural net is trained on samples that are
    # always from player 1's perspective (always from
    # maximizing player's perspective), so we
    # need to do a transformation if it is currently player 2's turn.
    if state.player == 1:
        policy_dist = pol( [state.toFeatures()] )[0]
    else:
        policy_dist = pol( [state.toFlippedFeatures()] )[0]
    n_acts = policy_dist.shape[0]

    # act = index which is also the action
    # TODO check if this try-until-success is faster than
    # checking for illegal actions first
    act = None
    while act is None or not state.isLegal(act):
        r = random.random()
        for a in xrange(n_acts):
            v = policy_dist[a]
            if r < v:
                act = a
                break
            r -= v
    return act


def makeSimpleTreePolicy(pol):
    """
    choose entirely based on the randomized policy (disregards both exploration
    parameter and online Q-value)

    Disregarding all online knowledge feels dumb,
    but using this would give an intuitive and easily computable next_move_distribution.

    (If the move selection policy is fixed for the entire MCTS,
    we get a very straightforward joint distribution P(select move && win with that move)
    which gives a straightforward P(select move | win) distribution.
    We can think of this as a better move distribution to use in the next iteration,
    since this is how a winning player would act in this game.)

    Note that under this policy, we are essentially doing a Monte Carlo
    evaluation of a fixed policy and then doing a policy update based
    on that evaluation. This feels too dumb/simple to work, but maybe
    it will?
    """

    # note the the tree policy is only considered when at a node
    # with no untried actions, thus all child nodes
    # are guaranteed to exist
    def treePol(node, state):
        a = chooseFromPolicy(pol, state)
        state.doAction(a)
        return node.childMap[a]
    return treePol

# given a gamestate, decide how to do a playout
def defaultDefaultPolicy(gameState):
    # make all moves randomly until terminal state is hit
    nacts = 0
    while not gameState.isTerminal():
        gameState.doAction(gameState.getRandomAction())
        nacts += 1
    r = gameState.getReward()
    #print nacts
    gameState.reverseActions(nacts)
    return r

def makeDefaultPolicy(policy):
    def default(gameState):
        nacts = 0
        while not gameState.isTerminal():
            gameState.doAction(policy(gameState))
            nacts += 1
        r = gameState.getReward()
        gameState.reverseActions(nacts)
        return r
    return default

def makeDefaultFromStochastic(policy):
    # policy takes a game state and returns a distribution
    # over what output move to take
    # the neural net is trained on samples that are
    # always from player 1's perspective (always from
    # maximizing player's perspective), so we
    # need to do a transformation if it is currently player 2's turn.
    # (done in chooseFromPolicy)
    def default(gameState):
        nacts = 0
        while not gameState.isTerminal():
            gameState.doAction(chooseFromPolicy(policy, gameState))
            nacts += 1
        r = gameState.getReward()
        gameState.reverseActions(nacts)
        return r
    return default


# after a payout, update game tree
# (Don't expect this to change, but don't lose that much clarity by doing this)
def defaultBackupPolicy(node, playoutReward):
    while node is not None:
        node.visits += 1
        node.qvalue += playoutReward
        node = node.parent

# Successive rejects - 
# passed in itermax
# On first level, reject arms after some number of rounds
# On all subsequent levels use UCT to get a playout
# (Bandit doesn't assume you have a tree you're building)
# Upside of UCT - guides along tree the whole way
# This doesn't lend itself as well to game tree search sampling
# Only use the game tree to get a more accurate playout of the game

# This can't be functional and needs to have state, unfortunately
# (Skeleton code assumes more functional)

def logbar(K):
    # Partial sum of harmonic series
    return 0.5 + sum(1.0 / i for i in xrange(2, K+1))

def successiveRejects(
    rootstate,
    itermax,
    treePolicy = defaultTreePolicy,
    defaultPolicy = defaultDefaultPolicy,
    backupPolicy = defaultBackupPolicy,
    ):
    """
    Return best move from root state when using successive rejects
    """
    rootnode = Node(state = rootstate)
    actions = set(rootstate.getActions())
    removed = set()
    num_actions = len(actions)
    print actions
    # Fixing an edge case
    if len(actions) == 1:
        return actions.pop()
    for action in actions:
        rootnode.addChildForAction(action)

    def n_k(k):
        if k == 0:
            return 0
        return int(ceil(1 / logbar(num_actions) * (itermax - num_actions) / (num_actions + 1 - k)))

    for round in xrange(1, num_actions):
        print "Reject round %d" % round
        playouts = n_k(round) - n_k(round-1)
        print "Attempting each action %d times" % playouts
        # play each action this many times
        for action in actions:
            # Do regular UCT for the estimate
            for _ in xrange(playouts):
                node = rootnode.getChildForAction(action)
                # Select (tree policy)
                while node.untriedActions == [] and not node.state.isTerminal(): # node is fully expanded and non-terminal
                    node = treePolicy(node)

                # Expand (add child node)
                if node.untriedActions != []: # if we can expand (i.e. state/node is non-terminal)
                    node = node.addChildForAction(random.choice(node.untriedActions))

                # Rollout
                # double check - can we get to here if the state is terminal, and will algorithm get stuck here?
                state = node.state
                endState = defaultPolicy(state)

                # Backpropagate
                backupPolicy(node, endState.getReward())
        # Reject worst arm
        print rootnode.ChildrenToString()

        worstAction = rootnode.leastWins(banned=removed)
        print "Removing %d" % (worstAction)
        removed.add(worstAction)
        actions.remove(worstAction)


    print "Player %d's move" % (rootnode.getPlayer())
    print rootnode.ChildrenToString()

    print "Actions, should only be one, %s" % (actions)
    return actions.pop()


def UCT(
    rootstate,
    itermax,
    treePolicy = defaultTreePolicy,
    defaultPolicy = defaultDefaultPolicy,
    backupPolicy = defaultBackupPolicy,
    returnValues=False,
    valueFunc=None,
    netTrainer=None,
    ):
    """
    Conduct a UCT search for itermax iterations starting from rootstate.
    Return the best move from the rootstate.
    In theory this should handle both maximizing and minimizing player
    """
    rootnode = Node(state = rootstate)
    state = rootstate

    for i in xrange(itermax):
        # make sure code isn't broken
        node = rootnode
        nacts = 0

        # Select (tree policy)
        while node.untriedActions == [] and not state.isTerminal(): # node is fully expanded and non-terminal
            node = treePolicy(node, state)
            nacts += 1

        # Expand (add child node)
        if node.untriedActions != []: # if we can expand (i.e. state/node is non-terminal)
            node = node.addChildForAction(random.choice(node.untriedActions), state)
            nacts += 1

        # Rollout
        # The default policy is assumed to restore the state
        # to its position before the rollout is started
        reward = defaultPolicy(state)

        # Backpropagate
        backupPolicy(node, reward)
        state.reverseActions(nacts)
        #print "Playout %d" % i

    print "Player %d's move" % (rootnode.getPlayer())
    print rootnode.ChildrenToString()

    if returnValues:
        vals = np.zeros( (7,) , dtype=theano.config.floatX)
        # we use -inf to signify an illegal action
        # this will naturally make the softmax of that value be 0.

        not_set = set(xrange(7))
        for node in rootnode.childNodes:
            vals[node.lastAction] = float(node.qvalue) / node.visits
            not_set.remove(node.lastAction)

        for i in not_set:
            vals[i] = float('-inf')

        return vals, rootnode.mostWins()
    if valueFunc is not None:
        # do SGD on values
        nodes = rootnode.childNodes
        random.shuffle(nodes)
        for node in nodes:
            act = node.lastAction
            state.doAction(act)
            valueFunc.valUpdate(state.toFeatures(),
                                float(node.qvalue) / node.visits)
            state.reverseAction()
    if netTrainer is not None:
        # do SGD on the values. Treat all Q values at once as a batch
        nodes = rootnode.childNodes
        random.shuffle(nodes)
        num_childs = len(nodes)

        x = []
        y = []
        for node in nodes:
            act = node.lastAction
            state.doAction(act)
            x.append(state.toFeatures())
            y.append(float(node.qvalue) / node.visits)
            state.reverseAction()
        netTrainer( np.array(x), np.array(y).reshape((-1, 1)) )

    return rootnode.mostWins()

def aggrevate(
    rootstate,
    itermax,
    defaultPolicy,
    ):
    # from rootstate, travel to a random child state
    # from there, do itermax rollouts, then return Q-values from those states
    # horizon is not exactly 42 when terminal, use something a bit looser
    horizon = 42 - len(rootstate.history)
    state = randomStopPoint(rootstate, horizon)

    vals = np.zeros( (7,) , dtype=theano.config.floatX)
    acts = set(state.getActions())
    for a in xrange(7):
        if a not in acts:
            vals[a] = float('-inf')
        else:
            for _ in xrange(itermax // len(acts)):
                state.doAction(a)
                vals[a] += defaultPolicy(state)
                state.reverseAction()
            vals[a] = vals[a] / float(itermax // len(acts))
    return state, vals


def neural_self_train(args, net_params_file_path):
    net_trainer, value_func, params = get_untrained_net()
    very_start = time.time()
    for _ in xrange(args.n):
        start_time = time.time()
        curr = 1
        state = ConnectFourState(player=curr)

        if args.vf_pol == 'greedy':
            pol = makeDefaultPolicy(makeEpsilonGreedyPolicyFromVF(value_func, epsilon=float(args.eps)))
        else:
            pol = makeDefaultPolicy(makeSoftmaxPolicyFromVF(value_func, beta = float(args.smb1)))

        while not state.isTerminal():
            t = time.time()
            action = UCT(state,
                         NUM_PLAYOUTS,
                         treePolicy=standardUCT,
                         defaultPolicy=pol,
                         netTrainer=net_trainer)
            state = state.doAction(action)
            print "%f seconds" % (time.time()-t)
            curr = 3 - curr
        print 'Game time %f seconds' % (time.time() - start_time)

    print 'Total time %f seconds' % (time.time() - very_start)
    # Dump parameters
    with open(net_params_file_path, 'wb') as f:
        for t_variable in params:
            cPickle.dump(t_variable.get_value(borrow=True), f, -1)

def normsample(state, vals, curr):
    if curr == 1:
        feats = state.toFeatures()
    else:
        feats = state.toFlippedFeatures()
        for i in xrange(len(vals)):
            if vals[i] != float('-inf'):
                vals[i] *= -1
    return feats, vals

def softmax(vals, policy_dist=None, beta=10):
    # policy_dist is unused and is only here to match the method signature
    for i in xrange(vals.shape[0]):
        vals[i] = np.exp(beta * vals[i])
    return np.true_divide(vals, np.sum(vals))

def conditionals(vals, policy_dist):
    # this is basically soft-EM, isn't it...
    # first convert Q values to probabilities
    # +1 on win and -1 on loss, so value 0 is actually 50%
    # fraction w are wins, fraction l are losses.
    # n = number playouts (we see this will cancel out)
    # q = (nw-nl) / n => q = w - l
    # with w + l = 1, get q = w - (1-w) = 2w - 1
    # so w = (q + 1) / 2
    # (we treat draws as 0.5 wins and 0.5 losses, gives same result)
    for i in xrange(vals.shape[0]):
        # check for -inf to denote illegal moves
        if vals[i] == float('-inf'):
            vals[i] = 0
        else:
            vals[i] = 0.5 * (vals[i] + 1)
    # now, vals[i] = P(win | move i picked)
    joint = vals * policy_dist
    # need to check the edge case where all moves lead to a loss
    if np.sum(joint) == 0:
        return joint
    return np.true_divide(joint, np.sum(joint))

def train_stochastic_neural_policy(args, save_file_path):
    very_start = time.time()
    net_trainer, stochastic_policy, params = get_kl_net_to_train()

    treepol = None
    # transforms the Q-values from MCTs into a stochastic policy to approximate
    transformer = None

    if args.treepol == 'UCTVF':
        treepol = makeUCTTreePolicy(stochastic_policy, args.UCT_param)
        transformer = softmax
    elif args.treepol == 'policyonly':
        treepol = makeSimpleTreePolicy(stochastic_policy)
        transformer = conditionals
    elif args.treepol == 'depthUCT':
        treepol = depthUCT
        transformer = softmax
    elif args.treepol == 'UCTVF_policy_hybrid':
        # use the same logic to choose child nodes in MCTs,
        # but use the policy-only logic to find the true distribution
        treepol = makeUCTTreePolicy(stochastic_policy, args.UCT_param)
        transformer = conditionals
    elif args.treepol == 'random':
        treepol = defaultTreePolicy
        transformer = softmax
    elif args.treepol == 'aggrevate':
        default = makeDefaultFromStochastic(stochastic_policy)
        costs = []
        best_avg = 10 ** 6
        epochs = 0
        patience = 8
        patience_increase = 8
        # load aggrevate test run
        with open(save_file_path) as f:
            for t_variable in params:
                t_variable.set_value(cPickle.load(f))
        while True:
            if epochs > patience:
                break
            epochs += 1
            for _ in xrange(1000):
                t = time.time()
                root = ConnectFourState(player=1)
                state, vals = aggrevate(root, NUM_PLAYOUTS, default)
                feats, vals = normsample(state, vals, state.player)
                net_dist = stochastic_policy( [feats] )[0]
                true_dist = softmax(vals, net_dist)
                assert (sum(true_dist) == 0) or (abs(sum(true_dist)-1) <= 10 ** -6), 'probability distribution has sum %f, dist=%s' % (sum(true_dist), str(true_dist))
                costs.append(net_trainer( [feats], [true_dist] ))
                print "%f seconds to get sample" % (time.time()-t)
            avg_cost = sum(costs) / float(len(costs))
            print 'Average cost: %f' % avg_cost
            if avg_cost < best_avg:
                patience += patience_increase
                # dump best model so far
                with open(save_file_path, 'wb') as f:
                    for t_variable in params:
                        cPickle.dump(t_variable.get_value(borrow=True), f, -1)
                best_avg = avg_cost
            with open("aggrevate_costs", 'ab') as f:
                cPickle.dump(costs, f, -1)
            costs = []
            print 'Epochs = %d, patience = %d, best_avg = %f' % (epochs, patience, best_avg)
        return




    for _ in xrange(args.n):
        start_time = time.time()
        curr = 1
        state = ConnectFourState(player=curr)
        costs = []
        # For training, consider batching all the distributions, instead of
        # processing one sample at a time.
        while not state.isTerminal():
            t = time.time()
            vals, action = UCT(state,
                         NUM_PLAYOUTS,
                         treePolicy=treepol,
                         defaultPolicy=makeDefaultFromStochastic(stochastic_policy),
                         returnValues=True,
                    )
            # train the net on this distribution
            feats, vals = normsample(state, vals, curr)
            # obtain a goal distribution and run on it
            net_dist = stochastic_policy( [feats] )[0]
            true_dist = transformer(vals, net_dist)
            # the sum == 0 check is for when all moves lead to a loss
            # in this scenario the cost is always 0 since there is no reasonable joint distribution
            # additionally the gradient is always 0 so this basically ignore this sample
            # , and that's fine (there's nothing to learn when there's no choice)
            assert (sum(true_dist) == 0) or (abs(sum(true_dist)-1) <= 10 ** -6), 'probability distribution has sum %f, dist=%s' % (sum(true_dist), str(true_dist))
            # net_trainer returns the cost and automatcally updates with a gradient
            costs.append(net_trainer( [feats] , [true_dist] ))

            state = state.doAction(action)
            print "Action %d, %f seconds" % (action, time.time()-t)
            #print state
            curr = 3 - curr
        print 'Game time %f seconds' % (time.time() - start_time)
        print 'Average cost: %f' % (sum(costs) / float(len(costs)))

    # dump params
    with open(save_file_path, 'wb') as f:
        for t_variable in params:
            cPickle.dump(t_variable.get_value(borrow=True), f, -1)

def refine_stochastic_neural_policy(args, load_file_path, model_file_path):
    very_start = time.time()
    net_trainer, stochastic_policy, params = get_kl_net_to_train()

    costs_file_path = model_file_path + "_costs"

    treepol = None
    # transforms the Q-values from MCTs into a stochastic policy to approximate
    transformer = None

    if args.treepol == 'UCTVF':
        treepol = makeUCTTreePolicy(stochastic_policy, args.UCT_param)
        transformer = softmax
    elif args.treepol == 'policyonly':
        treepol = makeSimpleTreePolicy(stochastic_policy)
        transformer = conditionals
    elif args.treepol == 'depthUCT':
        treepol = depthUCT
        transformer = softmax
    elif args.treepol == 'UCTVF_policy_hybrid':
        # use the same logic to choose child nodes in MCTs,
        # but use the policy-only logic to find the true distribution
        treepol = makeUCTTreePolicy(stochastic_policy, args.UCT_param)
        transformer = conditionals
    elif args.treepol == 'random':
        treepol = defaultTreePolicy
        transformer = softmax
    default = makeDefaultFromStochastic(stochastic_policy)
    game_costs = []
    best_avg = 10 ** 6
    games = 0
    patience = 100
    patience_increase = 50
    # load previous test run
    with open(load_file_path) as f:
        for t_variable in params:
            t_variable.set_value(cPickle.load(f))
    while True:
        if games > patience:
            break
        games += 1
        game_start = time.time()
        curr = 1
        state = ConnectFourState(player=curr)
        costs = []
        # For training, consider batching all the distributions, instead of
        # processing one sample at a time.
        while not state.isTerminal():
            t = time.time()
            vals, action = UCT(state,
                         NUM_PLAYOUTS,
                         treePolicy=treepol,
                         defaultPolicy=makeDefaultFromStochastic(stochastic_policy),
                         returnValues=True,
                    )
            # train the net on this distribution
            feats, vals = normsample(state, vals, curr)
            # obtain a goal distribution and run on it
            net_dist = stochastic_policy( [feats] )[0]
            true_dist = transformer(vals, net_dist)
            # the sum == 0 check is for when all moves lead to a loss
            # in this scenario the cost is always 0 since there is no reasonable joint distribution
            # additionally the gradient is always 0 so this basically ignore this sample
            # , and that's fine (there's nothing to learn when there's no choice)
            assert (sum(true_dist) == 0) or (abs(sum(true_dist)-1) <= 10 ** -6), 'probability distribution has sum %f, dist=%s' % (sum(true_dist), str(true_dist))
            # net_trainer returns the cost and automatcally updates with a gradient
            costs.append(net_trainer( [feats] , [true_dist] ))

            state = state.doAction(action)
            print "Action %d, %f seconds" % (action, time.time()-t)
            #print state
            curr = 3 - curr
        print 'Game time %f seconds' % (time.time() - game_start)
        game_costs.append( sum(costs) / float(len(costs)) )
        print 'Average cost: %f' % game_costs[-1]
        if games % 10 == 0:
            # check the average for a patience increase
            print '%d games completed' % games
            batch_avg = sum(game_costs) / float(len(game_costs))
            if batch_avg < best_avg:
                best_avg = batch_avg
                patience += patience_increase
                print 'Patience increased to %d' % patience
                with open(model_file_path, 'wb') as f:
                    for t_variable in params:
                        cPickle.dump(t_variable.get_value(borrow=True), f, -1)
            with open(costs_file_path, 'ab') as f:
                cPickle.dump(game_costs, f, -1)
            game_costs = []
    print 'Total training time %f seconds' % (time.time() - very_start)

def sample_learn_games(args):
    print 'Learning with ' + args.vf_pol
    with open(args.vf_file) as f:
        vf_weights = eval(f.read().replace("\n", ""))
    valueFunc = LinearValueFunction(
        vf_weights[:-1],
        vf_weights[-1],
        alpha=0.001,
        comp_hyper=0.005,
        epsilon=0.2,
    )
    vf_pol = args.vf_pol
    epsilon_step = 0
    if vf_pol == 'moving_greedy' or vf_pol == 'moving_greedy_softmax':
        valueFunc.epsilon = 1.0
        epsilon_step = (1.0 - 0.2) / args.n

    very_start = time.time()
    results = []
    for _ in xrange(args.n):
        start_time = time.time()
        curr = 1
        state = ConnectFourState(player=curr)
        if vf_pol in ('fixed_greedy', 'moving_greedy'):
            pol = makeDefaultPolicy(valueFunc.getEpsilonGreedyAction)
        elif vf_pol in ('fixed_greedy_softmax', 'moving_greedy_softmax'):
            pol = makeDefaultPolicy(valueFunc.getEpsilonProportionalAction)
        while not state.isTerminal():
            t = time.time()
            action = UCT(state,
                         NUM_PLAYOUTS,
                         treePolicy=standardUCT,
                         defaultPolicy=pol,
                         valueFunc=valueFunc)
            state = state.doAction(action)
            print "%f seconds" % (time.time()-t)
            curr = 3 - curr
            print state
        # dump parameters
        with open(args.vf_file, 'w') as f:
            print >> f, valueFunc.getParams()
        # update epsilon
        valueFunc.epsilon -= epsilon_step
        print 'Value function epsilon to %f' % (valueFunc.epsilon)
        winner = state.getWinner()
        print 'Player %d wins' % state.getWinner()
        print 'Game time %f seconds' % (time.time() - start_time)
        results.append(winner)

    print 'Total time %f seconds' % (time.time() - very_start)
    print 'Results: %s\n%d wins Player 1\n%d wins Player 2\n%d draws' % (str(results), results.count(1), results.count(2), results.count(0))
    print valueFunc.getParams()


def sample_game(args):
# simple loop to play game against AI
# should be doing self-play
    # interested to see if this changes things
    random.seed()
    print 'Weights for P1 if linear', args.p1w
    print 'Weights for P2 if linear', args.p2w
    print 'Beta for P1 if softmax', args.smb1
    print 'Beta for P2 if softmax', args.smb2
    weights1 = eval(open(args.p1w).read().replace("\n", ""))
    weights2 = eval(open(args.p2w).read().replace("\n", ""))
    curr = 1
    Games = {'cfour': ConnectFourState, 'ttt': TicTacToeState, 'go': GoState}
    state = Games[args.game](player=curr)
    policies = {
        'random': defaultTreePolicy,
        'UCT': standardUCT,
        'UCTdepth': depthUCT,
        'depthUCT': depthUCT,
        'human': None,
    }

    # hacky and bad fix. Change later
    beta1 = int(args.smb1) if args.smb1 is not None else None
    beta2 = int(args.smb2) if args.smb2 is not None else None
    playoutPolicies = {
        'random': [defaultDefaultPolicy, defaultDefaultPolicy],
        'linear': [makeDefaultPolicy(makeEpsilonGreedyPolicyFromWeights(weights1, epsilon=0.2)),
                   makeDefaultPolicy(makeEpsilonGreedyPolicyFromWeights(weights2, epsilon=0.2))],
        'softmax': [makeDefaultPolicy(makeSoftmaxPolicy(weights1, beta=beta1)),
                    makeDefaultPolicy(makeSoftmaxPolicy(weights2, beta=beta2))],
        'stochastic': [None, None],
    }
    # ONLY SUPPORTS ONE STOCHASTIC NET AT A TIME
    # FIX THIS SOON
    if args.p1 == 'sto_neural':
        if 'sto_neural' not in policies:
            policies['sto_neural'] = [None] * 2
        print 'Clobbering the given default policy 1 if it was given!!!'
        args.p1d = 'stochastic'
        sto_policy_1 = load_kl_net(args.p1loadfrom)

        print 'Sanity check'
        print sto_policy_1( [ConnectFourState().toFeatures()] )

        if args.p1treepol == 'UCTVF':
            policies['sto_neural'][0] = makeUCTTreePolicy(sto_policy_1, args.UCT_param)
        elif args.p1treepol == 'policyonly':
            policies['sto_neural'][0] = makeSimpleTreePolicy(sto_policy_1)
        else:
            policies['sto_neural'][0] = policies[args.p1treepol]
        playoutPolicies['stochastic'][0] = makeDefaultFromStochastic(sto_policy_1)

    if args.p2 == 'sto_neural':
        if 'sto_neural' not in policies:
            policies['sto_neural'] = [None] * 2
        print 'Clobbering the given default policy 2 if it was given!!!'
        args.p2d = 'stochastic'
        sto_policy_2 = load_kl_net(args.p2loadfrom)

        print 'Sanity check'
        print sto_policy_2( [ConnectFourState().toFeatures()] )

        if args.p2treepol == 'UCTVF':
            policies['sto_neural'][1] = makeUCTTreePolicy(sto_policy_2, args.UCT_param)
        elif args.p2treepol == 'policyonly':
            policies['sto_neural'][1] = makeSimpleTreePolicy(sto_policy_2)
        else:
            policies['sto_neural'][1] = policies[args.p2treepol]
        playoutPolicies['stochastic'][1] = makeDefaultFromStochastic(sto_policy_2)


    start_time = time.time()
    while not state.isTerminal():
        t=time.time()
        if curr == 1:
            if args.p1 == 'rejects':
                if args.quickpatch1 == False:
                    action = successiveRejects(state, NUM_PLAYOUTS, treePolicy=standardUCT, defaultPolicy=playoutPolicies[args.p1d])
                else:
                    action = successiveRejects(state, NUM_PLAYOUTS, treePolicy=defaultTreePolicy, defaultPolicy=playoutPolicies[args.p1d])
            else:
                treepol = policies[args.p1]
                if type(treepol) == list:
                    treepol = treepol[0]
                default = playoutPolicies[args.p1d]
                if type(default) == list:
                    default = default[0]

                action = UCT(state, NUM_PLAYOUTS, treePolicy=treepol, defaultPolicy=default) if args.p1 != 'human' else input()
        else:
            if args.p2 == 'rejects':
                if args.quickpatch2 == False:
                    action = successiveRejects(state, NUM_PLAYOUTS, treePolicy=standardUCT, defaultPolicy=playoutPolicies[args.p2d])
                else:
                    action = successiveRejects(state, NUM_PLAYOUTS, treePolicy=defaultTreePolicy, defaultPolicy=playoutPolicies[args.p2d])
            else:
                treepol = policies[args.p2]
                if type(treepol) == list:
                    treepol = treepol[1]
                default = playoutPolicies[args.p2d]
                if type(default) == list:
                    default = default[1]

                action = UCT(state, NUM_PLAYOUTS, treePolicy=treepol, defaultPolicy=default) if args.p2 != 'human' else input()

        state = state.doAction(action)
        print "%f seconds to move, action=%d" % (time.time()-t, action)
        curr = 3 - curr
        print state
    winner = state.getWinner()
    print 'Player %d wins' % state.getWinner()
    print 'Total time %f seconds' % (time.time() - start_time)
    return winner

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='MCTS testing')
    parser.add_argument('--game', dest='game', action='store', default='cfour', help='Game to play (cfour only for now)')
    parser.add_argument('--p1', dest='p1', action='store', default='depthUCT', help='specify P1 policy (random, UCT, UCTbase, human)')
    parser.add_argument('--p2', dest='p2', action='store', default='depthUCT', help='specify P2 policy (random, UCT, UCTbase, human)')
    parser.add_argument('--p1playout', dest='p1d', action='store', default='random')
    parser.add_argument('--p2playout', dest='p2d', action='store', default='random')
    # even if the linear functions aren't used, this is still required to make things not break
    # I know it's dumb. I will fix this...hopefully soon
    parser.add_argument('--p1weights', dest='p1w', action='store', default='vf_weights1', help='specify name of file for weights, linear case only')
    parser.add_argument('--p2weights', dest='p2w', action='store', default='vf_weights1', help='specify name of file for weights, linear case only')
    parser.add_argument('--patch1', dest='quickpatch1', action='store', default=False)
    parser.add_argument('--patch2', dest='quickpatch2', action='store', default=False)
    parser.add_argument('-n', dest='n', action='store', default=1, type=int, help='If specified play this many and return results')
    parser.add_argument('--vfweightsfile', dest='vf_file', action='store', default='vf_weights')
    parser.add_argument('--vfplayoutpol', dest='vf_pol', action='store', default='fixed_greedy')
    # eps = epsilon
    parser.add_argument('--eps', dest='eps', action='store', default=0.2)
    # smb = softmax beta
    parser.add_argument('--smb1', dest='smb1', action='store', default=None)
    parser.add_argument('--smb2', dest='smb2', action='store', default=None)
    parser.add_argument('--dumpto', dest='dumpto', action='store', default=None)
    parser.add_argument('--loadfrom', dest='loadfrom', action='store', default='learning_theano/net_params')
    parser.add_argument('--treepol', dest='treepol', action='store', default=None)
    parser.add_argument('--UCT_param', dest='UCT_param', action='store', type=float, default=10.0)
    parser.add_argument('--p1loadfrom', dest='p1loadfrom', action='store', default='learning_theano/net_params')
    parser.add_argument('--p2loadfrom', dest='p2loadfrom', action='store', default='learning_theano/net_params')
    parser.add_argument('--p1treepol', dest='p1treepol', action='store', default='depthUCT')
    parser.add_argument('--p2treepol', dest='p2treepol', action='store', default='depthUCT')

    args = parser.parse_args()
    print 'Player 1 playout pol: ', args.p1d
    print 'Player 2 playout pol: ', args.p2d
    qw = time.time()
    if args.p1d == 'vf' or args.p2d == 'vf':
        sample_learn_games(args)
    elif args.p1d == 'nvf' or args.p2d == 'nvf':
        neural_self_train(args, args.dumpto)
    elif args.p1d == 'nsp' or args.p2d == 'nsp':
        train_stochastic_neural_policy(args, args.dumpto)
    elif args.p1d == 'refine':
        refine_stochastic_neural_policy(args, args.loadfrom, args.dumpto)
    elif args.n:
        results = []
        for _ in xrange(args.n):
            results.append(sample_game(args))
        print 'Results: %s\n%d wins Player 1\n%d wins Player 2\n%d draws' % (str(results), results.count(1), results.count(2), results.count(0))
    print time.time() - qw
