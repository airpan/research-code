# convert pybrain net into a pure numpy net
# (make it runnable in PyPy)
import platform
USING_PYPY = platform.python_implementation() == 'PyPy'
import numpy as np
import cPickle

class Linear:
    def __init__(self, weights, bias):
        self.weights = weights
        self.bias = bias

    def val(self, inputs):
        # pass numpy array
        return sum(inputs * self.weights) + self.bias

class Sigmoid:
    def __init__(self, weights, bias):
        self.weights = weights
        self.bias = bias

    def val(self, inputs):
        lin = sum(inputs * self.weights) + self.bias
        return 1.0 / (1.0 + np.exp(-lin))

class Net:
    def __init__(self, hidden, out):
        self.hidden = hidden
        self.out = out

    def activate(self, features):
        return self.out.val([neuron.val(features) for neuron in self.hidden])

if not USING_PYPY:
    from pybrain.datasets import SupervisedDataSet
    from pybrain.supervised import BackpropTrainer
    from pybrain.utilities import percentError
    from pybrain.tools.shortcuts import buildNetwork
    from pybrain.tools.xml.networkwriter import NetworkWriter
    from pybrain.tools.xml.networkreader import NetworkReader

    net = NetworkReader.readFrom('net_cfour.xml')
    connections = net.connections.values()
    connections = [[layer.params for layer in conn] for conn in connections if conn]
    test_data = cPickle.load(open("test_game_data"))
    err = 0
    for feat, value in test_data:
        err += (net.activate(feat)[0] - value) ** 2
    print 'MSE = %f' % (err / float(len(test_data)))

    net = NetworkReader.readFrom('net_cfour2.xml')
    err = 0
    for feat, value in test_data:
        err += (net.activate(feat)[0] - value) ** 2
    print 'Net 2 MSE = %f' % (err / float(len(test_data)))

    weights = eval(open("weights_try_2").read().replace("\n", ""))
    err = 0
    for feat, value in test_data:
        err += (weights[-1] + sum( a*b for a,b in zip(feat, weights[:-1])) - value) ** 2
    print 'Lin MSE = %f' % (err / float(len(test_data)))
    d = 1/0

    in_to_hidden = connections[0][0]
    hidden_to_out = connections[1][0]
    bias_out = connections[2][0]
    bias_hidden = connections[2][1]
    hidden = [Sigmoid(in_to_hidden[42*i:42*i + 42], bias_hidden[i]) for i in xrange(100)]
    out = Linear(hidden_to_out, bias_out[0])
    net2 = Net(hidden, out)
    print net2.activate(np.zeros(42))
    cPickle.dump(net2, open("net_in_pypy", "w"))
    d = 1 / 0

net2 = cPickle.load(open("net_in_pypy"))
import time
t = time.time()
for _ in xrange(1000):
    net2.activate(np.random.rand(42))
print time.time() - t
