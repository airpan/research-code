# helper methods that change value functions into randomized policies
import random
import math
from math import exp

def val(features, weights):
    # computes linear value function
    # assumes the last element of weights is a bias
    return sum(f*w for f,w in zip(features, weights[:-1])) + weights[-1]

def makeEpsilonGreedyPolicyFromWeights(weights, epsilon=0.2):
    def policy(state):
        if random.random() < epsilon:
            return state.getRandomAction()
        values = []
        for a in state.getActions():
            values.append( (a, val(state.doAction(a).toFeatures(), weights)) )
            state.reverseAction()
        if state.getPlayer() == 1:
            bestVal = max(values, key=lambda v: v[1])[1]
        else:
            bestVal = min(values, key=lambda v: v[1])[1]
        bestActions = [a for a,v in values if v == bestVal]
        return random.choice(bestActions)
    return policy

# HACK TO MAKE THE NEURAL NET WORK, DO THIS RIGHT LATER
def makeEpsilonGreedyPolicyFromVF(value_func, epsilon=0.2):
    def policy(state):
        if random.random() < epsilon:
            return state.getRandomAction()
        values = []
        for a in state.getActions():
            values.append( (a, value_func(state.doAction(a).toFeatures())[0] ) )
            state.reverseAction()
        if state.getPlayer() == 1:
            bestVal = max(values, key=lambda v: v[1])[1]
        else:
            bestVal = min(values, key=lambda v: v[1])[1]
        bestActions = [a for a,v in values if v == bestVal]
        return random.choice(bestActions)
    return policy

def makeSoftmaxPolicy(weights, beta):
    # current tuning - 
    # assume all actions besides the highest value have value equal to worst action
    # in this problem, choose beta such that highest value move picked 80% of the time
    # (different beta for each state)

    # or, specify a beta when called
    def policy(state):
        sgn = 1 if state.getPlayer() == 1 else -1
        values = []
        for a in state.getActions():
            values.append( (a, sgn * val(state.doAction(a).toFeatures(), weights)) )
            state.reverseAction()
        nA = len(values)
        if nA == 1:
            return values[0][0]
        if beta is None:
            # compute dynamically
            diff = max(values) - min(values)
            # e^{beta * diff} / (e^{beta * diff} + nA - 1) = 0.8
            # solving gives
            beta2 = math.log(4 * (nA - 1)) / diff
            # large beta cause overflow, set a cap of beta = 100
            beta2 = min(beta2, 100)
        else:
            beta2 = beta

        # yay for consistent iteration order!
        values = [(a, exp(beta2 * v)) for a,v in values]
        total = sum(v[1] for v in values)
        ran = random.random() * total
        for a, v in values:
            if ran < v:
                return a
            ran -= v
        raise ValueError("Should not get here")
    return policy

# SAME HERE
def makeSoftmaxPolicyFromVF(value_func, beta):
    def policy(state):
        sgn = 1 if state.getPlayer() == 1 else -1
        values = []
        for a in state.getActions():
            values.append( (a, sgn * value_func(state.doAction(a).toFeatures())[0] ) )
            state.reverseAction()
        nA = len(values)
        if nA == 1:
            return values[0][0]
        if beta is None:
            # compute dynamically
            diff = max(values) - min(values)
            # e^{beta * diff} / (e^{beta * diff} + nA - 1) = 0.8
            # solving gives
            beta2 = math.log(4 * (nA - 1)) / diff
            # large beta cause overflow, set a cap of beta = 100
            beta2 = min(beta2, 100)
        else:
            beta2 = beta

        # yay for consistent iteration order!
        values = [(a, exp(beta2 * v)) for a,v in values]
        total = sum(v[1] for v in values)
        ran = random.random() * total
        for a, v in values:
            if ran < v:
                return a
            ran -= v
        raise ValueError("Should not get here")
    return policy


