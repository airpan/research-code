import numpy as np
cimport numpy as np
import theano
import random
from libc.limits cimport INT_MIN

# Normally, this would inherit from GameState
# However, GameState actually does pretty much nothing
# the only method it implements is simple getters,
# so this reimplementes in the name of performance

cdef class GomokuState(object):
    cdef int player
    cdef np.ndarray boards
    cdef int reward
    DEF rows = 12
    DEF cols = 12
    cdef int history[rows * cols]
    cdef int nacts
    cdef int r
    cdef int c

    def __cinit__(self, int player=1):
        """
        Player = 1 or 2
        board: There is one board for each player
            Although this seems silly, we want two bitmaps as
            the features/inputs later on
        board = 0 for empty, 1 for player 1 piece, 2 for player 2 piece
        actions = a column number from 0 to 6
        reward = +1 for player 1 win, -1 for player 2 win, 0 for a draw

        0 1 2 3 4 5
        6 7 8 9 10 11
        and so on...
        """
        self.player = player
        self.boards = np.zeros( (2, rows, cols), dtype=theano.config.floatX)
        self.reward = INT_MIN
        self.nacts = 0
        self.r = rows
        self.c = cols

    def doAction(self, int action, int y=-1):
        cdef int x
        if y == -1:
            x, y = action // cols, action % cols
        else:
            x, y = action, y
        # actions are a single index of format x-coor + (y * cols)
        # modifies the state in place and returns self
        self.boards[self.player - 1][x][y] = 1
        self.player = 3 - self.player
        self.reward = INT_MIN
        self.history[self.nacts] = x * cols + y
        self.nacts += 1

        return self

    def reverseAction(self):
        # reverse the previous action
        prev_action = self.history[self.nacts-1]
        self.nacts -= 1 # Ordered this way for what IMO is better readability
        self.player = 3 - self.player
        self.boards[self.player-1][prev_action // cols][prev_action % cols] = 0
        return self

    def getActions(self):
        if self.isTerminal():
            return []
        cdef int i, j
        return [i * cols + j for i in xrange(rows) for j in xrange(cols) if self.isLegal(i * cols + j)]

    def getRandomAction(self):
        return random.choice(self.getActions())

    cpdef isLegal(self, int action):
        cdef int x, y
        x = action // cols
        y = action % cols
        return self.boards[0][x][y] == 0 and self.boards[1][x][y] == 0

    cpdef isTerminal(self):
        # only need to check last piece added
        if self.nacts < 5:
            return False
        cdef int i, j, x, y
        i = self.history[self.nacts-1]
        i, j = i // cols, i % cols

        # Horizontals
        # Note that first index picks the row
        for x in xrange(max(0, j-4), min(cols - 4, j+1)):
            if self.boards[2-self.player][i][x] == self.boards[2-self.player][i][x+1] == self.boards[2-self.player][i][x+2] == self.boards[2-self.player][i][x+3] == self.boards[2-self.player][i][x+4]:
            #    print 'ho'
                return True

        # Verticals
        for y in xrange(max(0, i-4), min(rows - 4, i+1)):
            if self.boards[2-self.player][y][j] == self.boards[2-self.player][y+1][j] == self.boards[2-self.player][y+2][j] == self.boards[2-self.player][y+3][j] == self.boards[2-self.player][y+4][j]:
            #    print 've'
                return True

        # Diag top left to bottom right
        # x and y are bottom most piece of diag
        y = i - 4
        x = j - 4
        while x < 0 or y < 0:
            x += 1
            y += 1
        while x + 4 < cols and y +4 < rows and x <= j and y <= i:
            if self.boards[2-self.player][y][x] == self.boards[2-self.player][y+1][x+1] == self.boards[2-self.player][y+2][x+2] == self.boards[2-self.player][y+3][x+3] == self.boards[2-self.player][y+4][x+4]:
            #   print 'di1'
                return True
            y += 1
            x += 1

        # Diag top right to bottom left
        y = i - 4
        x = j + 4
        while x >= cols or y < 0:
            x -= 1
            y += 1
        while x - 4 >= 0 and y + 4 < rows and x >= j and y <= i:
            if self.boards[2-self.player][y][x] == self.boards[2-self.player][y+1][x-1] == self.boards[2-self.player][y+2][x-2] == self.boards[2-self.player][y+3][x-3] == self.boards[2-self.player][y+4][x-4]:
            #    print 'di2'
                return True
            y += 1
            x -= 1

        # Draw case
        if self.nacts == rows * cols:
            self.reward = 0
            return True
        return False

    def getReward(self):
        # Zero sum game, 1 = win for maxmizer, -1 = win for minimizer
        # next player is losing player if this is terminal
        if self.reward != INT_MIN:
            return self.reward
        return 1 if self.player == 2 else -1

    def getRewardForAction(self, action):
        n = self.doAction(action)
        if not n.isTerminal():
            return 0
        return n.getReward()

    def getWinner(self):
        if self.getReward() == 0:
            return 0
        return 3 - self.player

    def toFeatures(self):
        return self.boards

    def toFlippedFeatures(self):
        # return the exact opposite convention as the above
        # use when we need to pretend player 2 is player 1
        return self.boards.roll(1, axis=0)

    def __str__(self):
        header = ['  ']
        for i in xrange(rows):
            header.append('%3d' % i)
        header = ''.join(header)
        to_print = [header]
        for i in xrange(rows):
            row = (self.boards[0][i][j] or -self.boards[1][i][j] for j in xrange(cols))
            row_st = []
            row_st.append('%3d' % i)
            row_st.extend('|.|' if c == 0 else '|O|' if c == 1 else '|X|' for c in row)
            to_print.append(''.join(row_st))
        return '\n'.join(to_print)
