# Use pybrain to get going fast (runs slowly)


from pybrain.datasets import SupervisedDataSet
from pybrain.supervised import BackpropTrainer
from pybrain.utilities import percentError
from pybrain.tools.shortcuts import buildNetwork
from pybrain.tools.xml.networkwriter import NetworkWriter
from pybrain.tools.xml.networkreader import NetworkReader

from cfour import ConnectFourState
from montecarlo import UCT

input_size = 42
output_size = 1
hidden_size = 100


net = buildNetwork(42, 100, 1)
ds = SupervisedDataSet(input_size, output_size)


print "Starting load"
count = 0
import cPickle
with open("train_game_data") as f:
    data = cPickle.load(f)
    for feat, value in data[:3234]:
        ds.addSample(feat, [value])
print len(ds)

print "Loaded samples"

trainer = BackpropTrainer(net, ds)
with open("temp", "w") as f:
    for i in xrange(1000):
        trainer.trainEpochs(1)
        f.write("%d\n" % i)
        f.flush()
# trainer.trainUntilConvergence()

NetworkWriter.writeToFile(net, 'net_cfour2.xml')
