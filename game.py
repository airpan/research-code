import random

class GameState:
    """
    A state of the game, i.e. the game board.
    Subclass this class and implement unimplemented methods
    """

    def __init__(self, player=1):
        """
        Overwrite this method to include desired game state
        Make sure to call super!
        """
        self.player = player
        self.reward = None # optional, sometimes helps to have this

    def doAction(self, action):
        """
        Returns a new game state representing the game after this move.
        """
        pass

    def getActions(self):
        """
        Get all possible moves from this state.
        """
        pass

    def getRandomAction(self):
        return random.choice(self.getActions())

    def getPlayer(self):
        # players are 1-indexed!
        return self.player

    def isTerminal(self):
        pass

    def getReward(self):
        pass

    def getWinner(self):
        # return the string
        pass
