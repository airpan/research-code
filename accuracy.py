# Tests the accuracy of the linear value function
# Uses a very simple data set - tests against games that have been
# completed.
# These games have fixed values -1, 0, 1

from cfour import ConnectFourState
from linear_td import LinearValueFunction

def test_accuracy(vf):
    NUM_TESTS = 10000
    err = 0
    for _ in xrange(NUM_TESTS):
        state = ConnectFourState()
        while not state.isTerminal():
            state.doAction(state.getRandomAction())
        exp = state.getReward()
        err += abs(exp - vf.getValue(state))
    return err / NUM_TESTS

for type in ('fg', 'mg', 'fgs', 'mgs'):
    weights = eval(open("value_func_experiments/%s" % (type)).read())
    vf = LinearValueFunction(weights[:-1], weights[-1])
    acc = []
    for j in xrange(5):
        acc.append(test_accuracy(vf))
        print '%s: %f' % (type, acc[-1])
    print 'Average: %f' % (sum(acc) / 5.0)

for type in ('fg', 'mg', 'fgs', 'mgs'):
    for i in xrange(1,3):
        weights = eval(open("robust_func_experiments/%s_%d" % (type, i)).read())
        vf = LinearValueFunction(weights[:-1], weights[-1])
        acc = []
        for j in xrange(5):
            acc.append(test_accuracy(vf))
            print '%s_%d: %f' % (type, i, acc[-1])
        print 'Average: %f' % (sum(acc) / 5.0)
