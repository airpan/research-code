import scipy.stats
import glob
import cPickle

import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

fontP = FontProperties()
fontP.set_size('small')

def avg_costs(names):
    legend = []
    for name in names:
        error = pull_costs(name)
        if error is None:
            continue
        if '0' in name:
            legend.append(name[:name.rfind('0')+1])
        elif 'converge' in name:
            legend.append(name[:name.rfind('converge')+8])
        else:
            legend.append(name)

        avg_error = sum(error) / float(len(error))
        print name, '%d games' % len(error)
        print "Average error:: %f" % avg_error
        step = 10
        error2 = [sum(error[i:i+step]) / float(len(error[i:i+step])) for i in xrange(0, len(error), step)]
        # last entry is noisy (not a full group of 10 games)
        error2[-1:] = []
        best = error2[0]
        print scipy.stats.linregress(range(len(error2)), error2)
        plt.xlabel("Number of training games")
        plt.ylabel("Avg KL divergence over entire game")
        plt.plot(range(0, step * len(error2), step), error2)
        plt.gca().set_ylim(bottom=0, top=1.4)
    # common starting substring
    i = 0
    while True:
        if len(set( name[:i] for name in names)) > 1:
            i -= 1
            break
        i += 1

    plt.title("Error for %s" % names[0][:i])
    plt.legend(legend, prop=fontP)
    plt.show()

def load_converge(path):
    """ ONLY FOR AGGREVATE"""
    costs = []
    with open(path) as f:
        try:
            while True:
                costs.extend(cPickle.load(f))
        except EOFError:
            pass
    return costs

def connect_converge(n1, n2):
    # connect the end of the start model costs with
    # convergence costs
    error = []
    with open(n1) as f:
        for line in f:
            if 'Average cost' in line:
                error.append(float(line.split("Average cost:")[1].strip()))

    with open(n2) as f:
        for line in f:
            if 'Average cost' in line:
                error.append(float(line.split("Average cost:")[1].strip()))

    return error

namenext = {'depthUCT': 'depthUCT2500fixed_out',
            'UCTVF': 'UCTVF2500fixed_out',
            'random': 'random2500_out',}

def pull_costs(name):
    if "converge" in name:
        return connect_converge(namenext[ name[:name.index("converge")] ], name)
    error = []
    with open(name) as f:
        for line in f:
            if 'Average cost' in line:
                error.append(float(line.split("Average cost:")[1].strip()))
    return error


names = glob.glob('depthUCT*_out')
avg_costs(names)
print '---------------------'
names = glob.glob('random*_out')
avg_costs(names)
print '---------------------'
names = glob.glob('UCTVF*_out')
avg_costs(names)
print '---------------------'

def load_aggrevate():
    err = load_converge("aggrevateconverge_errors")
    err.extend(load_converge("aggrevateconverge_errors2"))
    return err

def aggrevate():
    names = glob.glob("*aggrevate*_errors")
    di = dict()
    di["aggrevate_converge"] = load_aggrevate()
    for name in names:
        if "converge" in name:
            continue
        else:
            with open(name) as f:
                di[name] = cPickle.load(f)
    legend = list()
    for name in di:
        if '0' in name:
            legend.append(name[:name.rfind('0')+1])
        else:
            legend.append(name)

        vals = di[name]
        step = 100
        v = []
        while vals:
            v.append(sum(vals[:step]) / len(vals[:step]))
            vals[:step] = []
        # remove last value, noisy
        v[-1:] = []
        print name, len(v), "blocks of size %d each" % step
        print scipy.stats.linregress(range(len(v)), v)
        plt.plot(range(0, len(v) * step, step), v)
        plt.gca().set_ylim(bottom=0, top=1.4)
    plt.title("Error for AGGREVATE")
    plt.xlabel("Number of samples")
    plt.ylabel("KL divergence over samples")
    plt.legend(legend)
    plt.show()

aggrevate()
