from cfour import ConnectFourState
from montecarlo import UCT, makeDefaultPolicy, defaultDefaultPolicy, makeGreedyPolicyFromWeights, makeEpsilonGreedy
import cPickle
import random

def getData(f):
    # generate random playout then UCT estimate each one
    s = ConnectFourState()
    states = []
    # don't add final state by design
    while not s.isTerminal():
        states.append(s)
        s = s.doAction(s.getRandomAction())
    for state in states:
        # each UCT call makes a new tree
        # so there should be no state collision problems
        values, action = UCT(state, 1000, returnValues=True)
        f.write("%s\n" % str(values))
        f.flush()
    # last state is terminal, has known reward, but it should be caught in previous case
    # since we return the valeus of each child (so terminal nodes have fixed value)

def getUCTData(f, weights=None):
    # generateda for the plays from a UCT game
    # using random playouts
    s = ConnectFourState()
    if weights == None:
        pol = defaultDefaultPolicy
    else:
        pol = makeGreedyPolicyFromWeights(weights)
        pol = makeEpsilonGreedy(pol, epsilon=0.2)
        pol = makeDefaultPolicy(pol)

    while not s.isTerminal():
        values, action = UCT(s, 1000, defaultPolicy=pol, returnValues=True)
        f.write("%s\n" % str(values))
        f.flush()
        s = s.doAction(action)

def cleanData(f):
    samples = []
    for line in f:
        samples.extend(eval(line.replace("\n", "")))
    # 10 percent as test
    random.shuffle(samples)
    with open("iter_test_data", "w") as f:
        cPickle.dump(samples[:len(samples)/10], f)
    with open("iter_train_data", "w") as f:
        cPickle.dump(samples[len(samples)/10:], f)

weights = eval(open("weights_try_2").read().replace("\n", ""))
# output data to file game_data
with open("iter_game_data", "a") as f:
    for _ in xrange(50):
        getUCTData(f, weights=weights)
cleanData(open("iter_game_data"))
